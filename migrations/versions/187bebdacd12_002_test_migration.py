"""Test Migration

Revision ID: 187bebdacd12
Revises: 2532ad2de8f5
Create Date: 2015-01-12 19:13:53.342659

"""

revision = '187bebdacd12'
down_revision = '2532ad2de8f5'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('config', sa.Column('test', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('config', 'test')
