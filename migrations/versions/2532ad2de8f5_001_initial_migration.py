"""Initial Migration

Revision ID: 2532ad2de8f5
Revises: None
Create Date: 2015-01-12 01:31:03.125966

"""

# revision identifiers, used by Alembic.
revision = '2532ad2de8f5'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('part',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('category', sa.String(length=120), nullable=False),
    sa.Column('price', sa.Numeric(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('kit',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=120), nullable=False),
    sa.Column('stock', sa.Integer(), nullable=False),
    sa.Column('price', sa.Numeric(), nullable=False),
    sa.Column('location', sa.String(length=100), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('location')
    )
    op.create_table('event',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('content', sa.String(length=400), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tag',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Unicode(length=64), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('budget_category',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=120), nullable=True),
    sa.Column('type', sa.String(length=120), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('imagelink',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('link', sa.String(length=200), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users_user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=True),
    sa.Column('officer_title', sa.String(length=120), nullable=True),
    sa.Column('year', sa.String(length=120), nullable=True),
    sa.Column('facebookid', sa.String(length=120), nullable=True),
    sa.Column('bio', sa.Text(convert_unicode=True), nullable=True),
    sa.Column('major', sa.String(length=120), nullable=True),
    sa.Column('email', sa.String(length=120), nullable=True),
    sa.Column('password', sa.String(length=120), nullable=True),
    sa.Column('password_reset_token', sa.String(length=32), nullable=True),
    sa.Column('voting_token', sa.String(length=32), nullable=True),
    sa.Column('year_active', sa.Integer(), nullable=True),
    sa.Column('voted', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('name')
    )
    op.create_table('console',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('filename', sa.String(length=50), nullable=False),
    sa.Column('title', sa.String(length=50), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('discount',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.Column('amount', sa.Numeric(precision=10, scale=4), nullable=True),
    sa.Column('percent', sa.Float(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('photo',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('url', sa.String(length=2000), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('chip_type',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('unit',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('question',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('prompt', sa.Text(convert_unicode=True), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('position',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=False),
    sa.Column('multiple', sa.SmallInteger(), nullable=True),
    sa.Column('description', sa.Text(convert_unicode=True), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('manufacturer',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('cashbox_total',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('amount', sa.Numeric(precision=10, scale=4), nullable=False),
    sa.Column('date', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=300), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('subjects',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('category', sa.String(length=50), nullable=False),
    sa.Column('class_number', sa.String(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('vote',
    sa.Column('voter_id', sa.Integer(), nullable=False),
    sa.Column('candidate_id', sa.Integer(), nullable=False),
    sa.Column('position_id', sa.Integer(), nullable=False),
    sa.Column('write_in', sa.Text(convert_unicode=True), nullable=True),
    sa.ForeignKeyConstraint(['candidate_id'], [u'users_user.id'], ),
    sa.ForeignKeyConstraint(['position_id'], [u'position.id'], ),
    sa.ForeignKeyConstraint(['voter_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('voter_id', 'candidate_id', 'position_id')
    )
    op.create_table('kit_item',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('kit_id', sa.Integer(), nullable=True),
    sa.Column('quantity', sa.Integer(), nullable=False),
    sa.Column('part_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['kit_id'], ['kit.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('answer',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('question_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('position_id', sa.Integer(), nullable=True),
    sa.Column('answer', sa.Text(convert_unicode=True), nullable=False),
    sa.ForeignKeyConstraint(['position_id'], [u'position.id'], ),
    sa.ForeignKeyConstraint(['question_id'], [u'question.id'], ),
    sa.ForeignKeyConstraint(['user_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('post',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=120), nullable=True),
    sa.Column('content', sa.Text(), nullable=False),
    sa.Column('date', sa.BigInteger(), nullable=False),
    sa.Column('sticky', sa.Boolean(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('role_console',
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.Column('console_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['console_id'], ['console.id'], ),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], )
    )
    op.create_table('candidate',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('position_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['position_id'], ['position.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users_user.id'], )
    )
    op.create_table('finance_settings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('permissions', sa.String(length=128), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_subject',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('subject_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['subject_id'], ['subjects.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users_user.id'], )
    )
    op.create_table('transaction',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=120), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('budget_category_id', sa.Integer(), nullable=True),
    sa.Column('amount', sa.Numeric(precision=10, scale=4), nullable=True),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['budget_category_id'], [u'budget_category.id'], ),
    sa.ForeignKeyConstraint(['user_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('reimbursement',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('requester_id', sa.Integer(), nullable=True),
    sa.Column('amount', sa.Numeric(precision=10, scale=4), nullable=True),
    sa.Column('comment', sa.Text(convert_unicode=True), nullable=False),
    sa.Column('date_requested', sa.DateTime(), nullable=True),
    sa.Column('date_approved', sa.DateTime(), nullable=True),
    sa.Column('type', sa.Enum('REGULAR', 'CASHBOX', name='reimbursement_type'), nullable=True),
    sa.Column('status', sa.Enum('PENDING', 'APPROVED', 'COMPLETED', name='reimbursement_status'), nullable=True),
    sa.ForeignKeyConstraint(['requester_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('ic',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.Column('mftr_id', sa.Integer(), nullable=True),
    sa.Column('mftr_part_no', sa.String(), nullable=False),
    sa.Column('chip_type_id', sa.Integer(), nullable=True),
    sa.Column('location', sa.String(length=100), nullable=False),
    sa.Column('addtlInfo', sa.String(length=500), nullable=False),
    sa.Column('price', sa.Numeric(), nullable=False),
    sa.Column('stock', sa.Integer(), nullable=False),
    sa.Column('part_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['chip_type_id'], [u'chip_type.id'], ),
    sa.ForeignKeyConstraint(['mftr_id'], [u'manufacturer.id'], ),
    sa.ForeignKeyConstraint(['part_id'], [u'part.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('mftr_part_no')
    )
    op.create_table('config',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('voting_started', sa.Boolean(), nullable=True),
    sa.Column('lounge_open', sa.Boolean(), nullable=True),
    sa.Column('lounge_password', sa.String(length=120), nullable=True),
    sa.Column('officers_password', sa.String(length=120), nullable=True),
    sa.Column('voting_end_date', sa.DateTime(), nullable=True),
    sa.Column('landing_image_id', sa.Integer(), nullable=True),
    sa.Column('officers_image_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['landing_image_id'], [u'imagelink.id'], ),
    sa.ForeignKeyConstraint(['officers_image_id'], [u'imagelink.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('part_property',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('value', sa.Float(), nullable=True),
    sa.Column('primary', sa.Boolean(), nullable=False),
    sa.Column('unit_id', sa.Integer(), nullable=True),
    sa.Column('part_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['part_id'], [u'part.id'], ),
    sa.ForeignKeyConstraint(['unit_id'], [u'unit.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('time_slot',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('hour', sa.Integer(), nullable=False),
    sa.Column('day_of_week', sa.Enum('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', name='day_of_week'), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], [u'users_user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_role',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users_user.id'], )
    )
    op.create_table('project',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=120), nullable=False),
    sa.Column('url', sa.String(length=120), nullable=False),
    sa.Column('repo', sa.String(length=2083), nullable=True),
    sa.Column('description', sa.Text(), nullable=False),
    sa.Column('content', sa.Text(), nullable=False),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('image_link', sa.String(length=2083), nullable=True),
    sa.Column('image_caption', sa.Text(), nullable=True),
    sa.Column('completed', sa.Boolean(), nullable=True),
    sa.Column('project_contact_id', sa.Integer(), nullable=True),
    sa.Column('associated_tag_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['associated_tag_id'], ['tag.id'], ),
    sa.ForeignKeyConstraint(['project_contact_id'], ['users_user.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('title'),
    sa.UniqueConstraint('url')
    )
    op.create_table('post_imagelink',
    sa.Column('post_id', sa.Integer(), nullable=True),
    sa.Column('imagelink_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['imagelink_id'], ['imagelink.id'], ),
    sa.ForeignKeyConstraint(['post_id'], ['post.id'], )
    )
    op.create_table('receipt',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('seller_id', sa.Integer(), nullable=True),
    sa.Column('transaction_id', sa.Integer(), nullable=True),
    sa.Column('purchaser', sa.String(length=120), nullable=True),
    sa.Column('comment', sa.Text(convert_unicode=True), nullable=True),
    sa.ForeignKeyConstraint(['seller_id'], [u'users_user.id'], ),
    sa.ForeignKeyConstraint(['transaction_id'], [u'transaction.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('post_tags',
    sa.Column('post_id', sa.Integer(), nullable=True),
    sa.Column('tag_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['post_id'], ['post.id'], ),
    sa.ForeignKeyConstraint(['tag_id'], ['tag.id'], )
    )
    op.create_table('receipt_item',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('receipt_id', sa.Integer(), nullable=False),
    sa.Column('ieee_part_no', sa.Integer(), nullable=False),
    sa.Column('quantity', sa.Integer(), nullable=False),
    sa.Column('discount_id', sa.Integer(), nullable=True),
    sa.Column('discount_amount', sa.Numeric(precision=10, scale=4), nullable=True),
    sa.ForeignKeyConstraint(['discount_id'], [u'discount.id'], ),
    sa.ForeignKeyConstraint(['receipt_id'], [u'receipt.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('sponsor_type',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=400), nullable=True),
    sa.Column('min_donation', sa.Numeric(), nullable=False),
    sa.Column('max_donation', sa.Numeric(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('max_donation'),
    sa.UniqueConstraint('min_donation'),
    sa.UniqueConstraint('name')
    )
    op.create_table('sponsor_benefit',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('benefit', sa.Text(convert_unicode=True), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('benefit')
    )
    op.create_table('sponsor',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=400), nullable=True),
    sa.Column('company_website', sa.String(length=2000), nullable=True),
    sa.Column('logo', sa.String(length=2000), nullable=True),
    sa.Column('donation', sa.Numeric(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('sponsor_type_benefit',
    sa.Column('sponsor_type_id', sa.Integer(), nullable=True),
    sa.Column('sponsor_benefit_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['sponsor_benefit_id'], ['sponsor_benefit.id'], ),
    sa.ForeignKeyConstraint(['sponsor_type_id'], ['sponsor_type.id'], )
    )
    op.create_table('sponsor_profile',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('sponsor_id', sa.Integer(), nullable=True),
    sa.Column('banner', sa.String(length=2000), nullable=True),
    sa.Column('banner_y_offset', sa.Integer(), nullable=True),
    sa.Column('mission', sa.String(length=2000), nullable=True),
    sa.Column('description', sa.Text(convert_unicode=True), nullable=False),
    sa.ForeignKeyConstraint(['sponsor_id'], ['sponsor.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('description')
    )


def downgrade():
    op.drop_table('receipt_item')
    op.drop_table('post_tags')
    op.drop_table('receipt')
    op.drop_table('post_imagelink')
    op.drop_table('project')
    op.drop_table('user_role')
    op.drop_table('time_slot')
    op.drop_table('part_property')
    op.drop_table('config')
    op.drop_table('ic')
    op.drop_table('reimbursement')
    op.drop_table('transaction')
    op.drop_table('user_subject')
    op.drop_table('finance_settings')
    op.drop_table('candidate')
    op.drop_table('role_console')
    op.drop_table('post')
    op.drop_table('answer')
    op.drop_table('kit_item')
    op.drop_table('vote')
    op.drop_table('subjects')
    op.drop_table('role')
    op.drop_table('cashbox_total')
    op.drop_table('manufacturer')
    op.drop_table('position')
    op.drop_table('question')
    op.drop_table('unit')
    op.drop_table('chip_type')
    op.drop_table('photo')
    op.drop_table('discount')
    op.drop_table('console')
    op.drop_table('users_user')
    op.drop_table('imagelink')
    op.drop_table('budget_category')
    op.drop_table('tag')
    op.drop_table('event')
    op.drop_table('kit')
    op.drop_table('part')
    op.drop_table('sponsor_profile')
    op.drop_table('sponsor_type_benefit')
    op.drop_table('sponsor')
    op.drop_table('sponsor_benefit')
    op.drop_table('sponsor_type')
