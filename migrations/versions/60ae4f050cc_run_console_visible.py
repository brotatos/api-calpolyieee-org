"""run console visible

Revision ID: 60ae4f050cc
Revises: 187bebdacd12
Create Date: 2015-04-22 19:06:38.630338

"""

# revision identifiers, used by Alembic.
revision = '60ae4f050cc'
down_revision = '187bebdacd12'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('config', sa.Column('run_console_visible', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('config', 'run_console_visible')
