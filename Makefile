run: venv .env run.py
	find . -name '*.pyc' -delete
	. venv/bin/activate && foreman run python2 run.py

update_production_db: manage.py
	heroku run python2 manage.py db upgrade

flush_db: setup.sql
	psql -d ieee -c 'drop schema public cascade; create schema public;'
	foreman run python2 manage.py db upgrade
	psql -d ieee -a -f setup.sql

rebuild_db: dump flush_db restore

generate_migration: dump migrate_db restore

migrate_db: dump rebuild_db restore
	foreman run python2 manage.py db migrate

dump:
	pg_dump --data-only ieee > local.db

restore: local.db
	psql ieee < local.db

clean:
	find -name '*.pyc' -delete

