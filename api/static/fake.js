"use strict";

var fake = {};

fake.setup = function () {
    var self = this;

    this.url = window.location.protocol + "//" + window.location.host;
    this.job = 0;

    this.$endpoint = $("#endpoint");
    this.$quantity = $("#quantity");
    this.$propIn = $("#propIn");
    this.$propValueIn = $("#propValueIn");
    this.$submit = $("div:first-of-type button");
    this.$results = $("ul");
    this.msg = function (job, msg) {
        self.$results.append("<li>[#" + job + " " +
                             new Date().toTimeString() + "] " + msg + "</li>");
    };

    this.$submit.click(function () {
        var job = self.job++, data = {}, url = self.url + self.$endpoint.val();

        self.msg(job, "Working...");
        data.count = parseInt(self.$quantity.val(), 10),
        data[self.$propIn.val()] = JSON.parse(self.$propValueIn.val());

        $.post(url, JSON.stringify(data)).done(function (data) {
            if (data.errors.length) {
                self.msg(job, "Request had the following errors: ");
                data.errors.forEach(function (e) { self.msg(job, e); });
            } else {
                self.msg(job, "Done.");
            }
        });
    });
};
