from api.core import db
from api.models.users import User, candidate_table


class Position(db.Model):
    """
    title:  the title of the position e.g. President
    multiple: how many people can be elected to this position, e.g. 2,
    for "Special events coordinator"
    description: a short description of the position
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    multiple = db.Column(db.SmallInteger, default=1)
    candidates = db.relationship('User', secondary=candidate_table)
    answers = db.relationship('Answer', cascade='all, delete, delete-orphan')
    description = db.Column(db.Text(convert_unicode=True))

    @staticmethod
    def sortable():
        return ['id', 'name', 'multiple']

    def __init__(self, name=None, multiple=1, description=None):
        self.name = name
        self.multiple = multiple
        self.description = description

    def __repr__(self):
        return '<Position %r>' % (self.name)

    def __unicode__(self):
        return self.name

    @property
    def serialize(self):
        return {
            'id': self.id,
            'title': self.name,
            'multiple': self.multiple,
            'description': self.description,
            'candidates': [i.candidate_serialize(self.id) for i
                           in self.candidates]
        }


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    answers = db.relationship('Answer', cascade='all, delete, delete-orphan')
    prompt = db.Column(db.Text(convert_unicode=True), nullable=False)

    @staticmethod
    def sortable():
        return ['id']

    def __init__(self, prompt=None):
        self.prompt = prompt

    def __repr__(self):
        return '<Question %r>' % (self.prompt)

    def __unicode__(self):
        return self.prompt

    @property
    def serialize(self):
        return {
            'id': self.id,
            'prompt': self.prompt
        }


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey(Question.id))
    question = db.relationship('Question')
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    position_id = db.Column(db.Integer, db.ForeignKey(Position.id))
    answer = db.Column(db.Text(convert_unicode=True), nullable=False)

    @staticmethod
    def sortable():
        return ['id']

    def __init__(self, question_id=None, user_id=None,
                 position_id=None, answer=None):
        self.question_id = question_id
        self.user_id = user_id
        self.position_id = position_id
        self.answer = answer

    def __repr__(self):
        return '<Answer %r>' % (self.answer)

    def __unicode__(self):
        return self.answer

    @property
    def serialize(self):
        return {
            'prompt': self.question.prompt,
            'id': self.id,
            'response': self.answer
        }


class Vote(db.Model):
    # If the voter chose  "write in" then candidate_id will be write_in user
    __tablename__ = 'vote'
    voter_id = db.Column(db.Integer, db.ForeignKey(User.id), primary_key=True)
    candidate_id = db.Column(db.Integer, db.ForeignKey(User.id),
                             primary_key=True)
    position_id = db.Column(db.Integer, db.ForeignKey(Position.id),
                            primary_key=True)

    def __init__(self, voter_id=None, position_id=None, candidate_id=None):
        self.voter_id = voter_id
        self.position_id = position_id
        self.candidate_id = candidate_id
