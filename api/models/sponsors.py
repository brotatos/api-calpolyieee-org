from api.core import db

sponsor_type_benefit_table = \
    db.Table('sponsor_type_benefit', db.Model.metadata,
             db.Column('sponsor_type_id', db.Integer,
                       db.ForeignKey('sponsor_type.id')),
             db.Column('sponsor_benefit_id', db.Integer,
                       db.ForeignKey('sponsor_benefit.id'))
             )


class SponsorType(db.Model):

    __tablename__ = 'sponsor_type'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400), unique=True)
    min_donation = db.Column(db.Numeric(), nullable=False, unique=True)
    max_donation = db.Column(db.Numeric(), nullable=True, unique=True)
    benefits = db.relationship('SponsorBenefit',
                               secondary=sponsor_type_benefit_table)

    def __repr__(self):
        return '<SponsorType %r>' % (self.name)

    def __unicode__(self):
        return self.name


class Sponsor(db.Model):

    __tablename__ = 'sponsor'
    # Minimum amount of donation in order to obtain a website profile.
    MIN_PROFILE_DONATION = 1000
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400), unique=True)
    company_website = db.Column(db.String(2000))
    # URL to company logo.
    logo = db.Column(db.String(2000))
    donation = db.Column(db.Numeric(), nullable=False, unique=False)
    sponsor_profile = db.relationship("SponsorProfile", backref="Sponsor")

    def __repr__(self):
        return '<Sponsor %r>' % (self.name)

    def __unicode__(self):
        return self.name


class SponsorBenefit(db.Model):

    __tablename__ = 'sponsor_benefit'
    id = db.Column(db.Integer, primary_key=True)
    benefit = db.Column(db.Text(convert_unicode=True), nullable=False,
                        unique=True)

    def __repr__(self):
        return '<SponsorBenefit %r>' % (self.benefit)

    def __unicode__(self):
        return self.benefit


class SponsorProfile(db.Model):

    __tablename__ = 'sponsor_profile'
    id = db.Column(db.Integer, primary_key=True)
    sponsor_id = db.Column(db.Integer, db.ForeignKey('sponsor.id'))
    banner = db.Column(db.String(2000))
    banner_y_offset = db.Column(db.Integer)
    # Company mission.
    mission = db.Column(db.String(2000))
    description = db.Column(db.Text(convert_unicode=True), nullable=False,
                            unique=True)

    def __repr__(self):
        return '<SponsorProfile %r>' % (self.id)

    def __unicode__(self):
        return 'SponsorProfile ' + str(self.id)
