from api.utils import unix_time, get_part_by_id
from api.core import db
from api.models.users import User


class BudgetCategory(db.Model):

        __tablename__ = 'budget_category'
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(120))
        type = db.Column(db.String(120))

        def __init__(self, name=None, type=None):
            self.name = name
            self.type = type

        @property
        def serialize(self):
            return {
                'name': self.name,
                'type': self.type
            }

        @staticmethod
        def sortable():
            return ['id', 'name', 'type']


class Transaction(db.Model):

        __tablename__ = 'transaction'
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(120))
        user = db.relationship('User')
        user_id = db.Column(db.Integer, db.ForeignKey(User.id))
        budget_category = db.relationship('BudgetCategory')
        budget_category_id = db.Column(db.Integer,
                                       db.ForeignKey(BudgetCategory.id))
        receipt = db.relationship('Receipt',
                                  cascade='all, delete,delete-orphan')
        amount = db.Column(db.Numeric(precision=10, scale=4))
        date = db.Column(db.DateTime())

        def __init__(self, user_id=None, name=None, budget_category=None,
                     amount=None, date=None):
            self.user_id = user_id
            self.name = name
            self.budget_category = budget_category
            self.amount = amount
            self.date = date

        @property
        def serialize(self):
            return {
                'id': self.id,
                'name': self.name,
                'date': unix_time(self.date),
                'budget_category': self.budget_category.name,
                'amount': int(self.amount * 100),
                'user_id': self.user_id,
                'user_name': self.user.name,
            }

        @staticmethod
        def sortable():
            return ['id', 'name', 'date', 'amount']


class Discount(db.Model):

    __tablename__ = 'discount'
    START_ID = 30000
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=False)
    amount = db.Column(db.Numeric(precision=10, scale=4), nullable=True)
    percent = db.Column(db.Float, nullable=True)

    def __init__(self, name=None, amount=None, percent=None):
        self.name = name
        self.amount = amount
        self.percent = percent

    def __repr__(self):
        return '<Discount %r Amount: %r>' % (self.name, str(self.amount))

    @staticmethod
    def sortable():
        return ['id', 'name', 'amount', 'percent']

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'amount': self.amount,
            'percent': self.percent
        }


class Receipt(db.Model):

    __tablename__ = 'receipt'
    id = db.Column(db.Integer, primary_key=True)
    seller = db.relationship('User')
    seller_id = db.Column(db.Integer, db.ForeignKey(User.id))
    transaction = db.relationship('Transaction')
    transaction_id = db.Column(db.Integer, db.ForeignKey(Transaction.id))
    purchaser = db.Column(db.String(120), nullable=True, unique=False)
    items = db.relationship('ReceiptItem',
                            cascade='all, delete, delete-orphan')
    comment = db.Column(db.Text(convert_unicode=True),
                        nullable=True, unique=False)

    def __init__(self, seller_id=None, transaction_id=None,
                 purchaser=None, comment=None):
        self.seller_id = seller_id
        self.transaction_id = transaction_id
        self.purchaser = purchaser
        self.comment = comment

    def __repr__(self):
        return '<Receipt Seller: %r Purchaser: %r>' %\
            (self.seller, self.purchaser)

    @staticmethod
    def sortable():
        return ['id', 'purchaser']

    @property
    def serialize(self):
        return {
            'id': self.id,
            'seller_id': self.seller_id,
            'seller_name': User.query.filter_by(id=self.seller_id)
            .first().name,
            'purchaser_name': self.purchaser,
            'total_price': int(self.transaction.amount * 100),
            'comment': self.comment,
            'items': [item.serialize for item in ReceiptItem.query
                      .filter_by(receipt_id=self.id)],
            'date': unix_time(self.transaction.date)
        }


class ReceiptItem(db.Model):

    __tablename__ = 'receipt_item'
    id = db.Column(db.Integer, primary_key=True)
    receipt = db.relationship('Receipt')
    receipt_id = db.Column(db.Integer, db.ForeignKey(Receipt.id),
                           nullable=False)
    ieee_part_no = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    discount = db.relationship('Discount')
    discount_id = db.Column(db.Integer, db.ForeignKey(Discount.id),
                            nullable=True)
    discount_amount = db.Column(db.Numeric(precision=10, scale=4),
                                nullable=True)

    @staticmethod
    def sortable():
        return ['id', 'ieee_part_no', 'quantity']

    def __init__(self, receipt_id=None, ieee_part_no=None, quantity=1,
                 discount_id=None, discount_amount=None):
        self.receipt_id = receipt_id
        self.ieee_part_no = ieee_part_no
        self.quantity = quantity
        self.discount_id = discount_id
        self.discount_amount = discount_amount

    def __repr__(self):
        return '<ReceiptItem %r %r>' % (str(self.ieee_part_no),
                                        str(self.quantity))

    @property
    def serialize(self):
        if self.discount:
            json = {
                'id': self.ieee_part_no,
                'quantity': 1,
                'category': 'DISCOUNT',
                'price': int(self.discount_amount * 100),
                'main_info': {'name': self.discount.name},
                'addtl_info': []
            }
        else:
            json = get_part_by_id(self.ieee_part_no).serialize
            json['quantity'] = self.quantity
        return json


class Reimbursement(db.Model):

        __tablename__ = 'reimbursement'
        id = db.Column(db.Integer, primary_key=True)
        requester = db.relationship('User')
        requester_id = db.Column(db.Integer, db.ForeignKey(User.id))
        amount = db.Column(db.Numeric(precision=10, scale=4))
        comment = db.Column(db.Text(convert_unicode=True),
                            nullable=False, unique=False)
        date_requested = db.Column(db.DateTime())
        date_approved = db.Column(db.DateTime())
        type = db.Column(db.Enum("REGULAR", "CASHBOX",
                                 name="reimbursement_type"))
        status = db.Column(db.Enum("PENDING", "APPROVED", "COMPLETED",
                                   name="reimbursement_status"))

        def __init__(self, requester_id=None, amount=None,
                     comment=None, type=None, date_requested=None,
                     date_approved=None, status="PENDING"):
            self.requester_id = requester_id
            self.amount = amount
            self.comment = comment
            self.type = type
            self.date_requested = date_requested
            self.date_approved = date_approved
            self.status = status

        @staticmethod
        def sortable():
            return ['id', 'date_requested', 'date_approved', 'type',
                    'status', 'amount']

        @property
        def serialize(self):
            if self.date_approved:
                date = unix_time(self.date_approved)
            else:
                date = ""

            return {
                'id': self.id,
                'requester_id': self.requester_id,
                'requester_name': User.query.filter_by(id=self.requester_id)
                .first().name,
                'amount': int(self.amount * 100),
                'comment': self.comment,
                'type': self.type,
                'date_requested': unix_time(self.date_requested),
                'date_approved': date,
                'status': self.status
            }


class CashboxTotal(db.Model):

        __tablename__ = 'cashbox_total'
        id = db.Column(db.Integer, primary_key=True)
        amount = db.Column(db.Numeric(precision=10, scale=4), nullable=False)
        date = db.Column(db.DateTime(), nullable=False)

        def __init__(self, amount=None, date=None):
            self.amount = amount
            self.date = date

        @staticmethod
        def sortable():
            return ['id', 'amount', 'date']

        @property
        def serialize(self):
            return {
                'id': self.id,
                'amount': int(self.amount * 100),
                'date': unix_time(self.date)
            }


class FinanceSettings(db.Model):

    __tablename__ = "finance_settings"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    user = db.relationship('User')
    permissions = db.Column(db.String(128), unique=False, nullable=False)

    def __init__(self, user=None, permissions=None):
        self.user = user
        self.permissions = permissions

    @property
    def serialize(self):
        return {
            'id': self.id,
            'user_id': self.user.id,
            'user_name': self.user.name,
            'permissions': self.permissions
        }
