from api.core import db, SECRET_KEY
from flask.ext.login import UserMixin
from itsdangerous import URLSafeTimedSerializer

import os

login_serializer = URLSafeTimedSerializer(SECRET_KEY)

user_subject_table = db.Table('user_subject', db.Model.metadata,
                              db.Column('user_id', db.Integer,
                                        db.ForeignKey('users_user.id')),
                              db.Column('subject_id', db.Integer,
                                        db.ForeignKey('subjects.id'))
                              )

user_role_table = db.Table('user_role', db.Model.metadata,
                           db.Column('user_id', db.Integer,
                                     db.ForeignKey('users_user.id')),
                           db.Column('role_id', db.Integer,
                                     db.ForeignKey('role.id'))
                           )

role_console_table = db.Table('role_console', db.Model.metadata,
                              db.Column('role_id', db.Integer,
                                        db.ForeignKey('role.id')),
                              db.Column('console_id', db.Integer,
                                        db.ForeignKey('console.id'))
                              )

candidate_table = db.Table('candidate', db.Model.metadata,
                           db.Column('user_id', db.Integer,
                                     db.ForeignKey('users_user.id'),
                                     nullable=False),
                           db.Column('position_id', db.Integer,
                                     db.ForeignKey('position.id'),
                                     nullable=False)
                           )


class User(db.Model, UserMixin):

    __tablename__ = 'users_user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    officer_title = db.Column(db.String(120))
    year = db.Column(db.String(120))
    facebookid = db.Column(db.String(120))
    bio = db.Column(db.Text(convert_unicode=True))
    major = db.Column(db.String(120))
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(120))
    password_reset_token = db.Column(db.String(32))
    voting_token = db.Column(db.String(32))
    year_active = db.Column(db.Integer, nullable=True, unique=False)
    voted = db.Column(db.Boolean())
    time_slots = db.relationship('TimeSlot', backref='User')
    roles = db.relationship('Role', secondary=user_role_table)
    positions = db.relationship('Position', secondary=candidate_table)
    classes = db.relationship('Subject', secondary=user_subject_table)
    answers = db.relationship('Answer',
                              cascade='all,delete,delete-orphan',
                              lazy='dynamic')
    # posts = db.relationship('Post', cascade='all,delete,delete-orphan')
    # receipts = db.relationship('Receipt', backref='User')
    # reimbursements = db.relationship('Reimbursement',
    #                                  cascade='all,delete,delete-orphan')
    # projects = db.relationship('Project', cascade='all,delete,delete-orphan')

    @staticmethod
    def sortable():
        return ['id', 'name', 'officer_title', 'year',
                'facebookid', 'major', 'email', 'year_active']

    def __init__(self, email=None, password=None):
        self.email = email
        self.password = password

    def generate_token(self):
        return os.urandom(16).encode('hex')

    def set_reset_password_token(self):
        self.password_reset_token = self.generate_token()

    def set_confirm_identity_token(self):
        self.voting_token = self.generate_token()

    # Flask-Login methods
    def get_auth_token(self):
        """Returns the user's authentication token."""
        data = [unicode(self.id), self.password]
        return login_serializer.dumps(data)

    def is_authenticated(self):
        return True

    @property
    def consoles(self):
        consoles = []

        for role in self.roles:
            for console in role.consoles:
                consoles.append(console)
        return [i.serialize for i in consoles]

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'title': self.officer_title,
            'year': self.year,
            'fb_id': self.facebookid,
            'bio': self.bio,
            'major': self.major,
            'email': self.email,
            'year_active': self.year_active,
            'roles': [role.serialize for role in self.roles],
            # 'classes': [subj.serialize for subj in self.classes]
        }

    def candidate_serialize(self, position_id):
        return {
            'id': self.id,
            'name': self.name,
            'year': self.year,
            'questions': [i.serialize for i in
                          self.answers.filter_by(position_id=position_id)]
        }

    def obtain_class_list(self):
        """
        Generates a list of classes the user can tutor in
        Ex. {"EE": [440], "CPE" : [101, 102]}
        """
        res = {}

        for subj in self.classes:
            res.setdefault(subj.category, []).append(subj.class_number)
        for key in res:
            res[key] = sorted(res[key])

        return {
            "uid": self.id,
            "classes": res
        }


class Subject(db.Model):

    __tablename__ = 'subjects'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(50), unique=False, nullable=False)
    class_number = db.Column(db.String(120), unique=False, nullable=False)

    @staticmethod
    def sortable():
        return ['id', 'category', 'class_number']

    def __unicode__(self):
        return self.category + ' ' + self.class_number

    def __repr__(self):
        return '<Subject %s%s>' % (self.category, self.class_number)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'category': self.category,
            'class_number': self.class_number
        }


class Role(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), unique=True, nullable=False)
    consoles = db.relationship('Console', secondary=role_console_table,
                               lazy='dynamic')

    @staticmethod
    def sortable():
        return ['id', 'name']

    def __init__(self, name=None):
        self.name = name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Role %s>' % self.name

    @property
    def serialize(self):
        return self.name


class Console(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(50), unique=False, nullable=False)
    title = db.Column(db.String(50), unique=False, nullable=False)
    description = db.Column(db.String(255), unique=False, nullable=False)

    def __init__(self, filename=None, title=None, description=None):
        self.filename = filename
        self.title = title
        self.description = description

    def __repr__(self):
        return '<Console %s>' % self.filename

    @staticmethod
    def sortable():
        return ['id', 'filename', 'title', 'description']

    @property
    def serialize(self):
        return {
            'filename': self.filename,
            'title': self.title,
            'description': self.description
        }


class TimeSlot(db.Model):

    __tablename__ = 'time_slot'
    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('User')
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    hour = db.Column(db.Integer,
                     db.CheckConstraint("hour > 0 AND hour < 24"),
                     nullable=False)
    day_of_week = db.Column(db.Enum("MONDAY", "TUESDAY", "WEDNESDAY",
                                    "THURSDAY", "FRIDAY", name="day_of_week"),
                            nullable=False)

    def __init__(self, user_id=None, hour=None, day_of_week=None):
        self.user_id = user_id
        self.hour = hour
        self.day_of_week = day_of_week

    def __unicode__(self):
        return self.day_of_week + " " + str(self.hour)

    def __repr__(self):
        return '<TimeSlot %r %r>' % (self.day_of_week, str(self.hour))

    @staticmethod
    def sortable():
        return ['id', 'hour', 'day_of_week']

    @property
    def serialize(self):
        return {
            'user_id': self.user.id,
            'user_name': self.user.name,
            'user_year': self.user.year,
            'hour': self.hour,
            'day_of_week': self.day_of_week
        }
