from api.core import db


class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(2000), unique=False)

    def __init__(self, url=None):
        self.url = url

    def __repr__(self):
        return '<Photo %s>' % self.url

    @staticmethod
    def sortable():
        return ['id', 'url']

    @property
    def serialize(self):
        return self.url
