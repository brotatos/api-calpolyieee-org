import pkgutil
import importlib
from flask import Flask, g, jsonify, render_template
from flask.ext.login import current_user
from core import db, login_manager, migrate, cors,\
    DEBUG, TESTING, SQLALCHEMY_DATABASE_URI, SECRET_KEY, CORS_ALLOW_HEADERS,\
    CORS_RESOURCES, CORS_SUPPORTS_CREDENTIALS
from models.users import User
from decorators import requires_debug
import routes

app = Flask(__name__)

app.config.update(
    DEBUG=DEBUG,
    TESTING=TESTING,
    SQLALCHEMY_DATABASE_URI=SQLALCHEMY_DATABASE_URI,
    SECRET_KEY=SECRET_KEY,
    CORS_ALLOW_HEADERS=CORS_ALLOW_HEADERS,
    CORS_RESOURCES=CORS_RESOURCES,
    CORS_SUPPORTS_CREDENTIALS=CORS_SUPPORTS_CREDENTIALS
)


def register_blueprints(app, package_name, package_path):
    for _, name, _ in pkgutil.iter_modules(package_path):
        m = importlib.import_module('.' + name, package_name)
        app.register_blueprint(getattr(m, 'blueprint'))

db.init_app(app)
login_manager.init_app(app)
migrate.init_app(app, db)
cors.init_app(app)
register_blueprints(app, 'api.routes', routes.__path__)


@app.route('/')
@requires_debug
def slash():
    return render_template('index.html')


@app.before_request
def before_request():
        g.user = current_user


@app.errorhandler(404)
def page_not_found(e):
    return jsonify(success=False, errors=['Page not found.']), 404


@app.errorhandler(403)
def forbidden(e):
    return jsonify(success=False, errors=['Not authorized.']), 403


@app.errorhandler(500)
def internal_error(e):
    return jsonify(success=False,
                   errors=['Something has gone horribly wrong.']), 500


@login_manager.user_loader
def load_user(id):
    return User.query.get(id)
