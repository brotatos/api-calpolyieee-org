import math
import re
import api.fake
from fuzzywuzzy import process
from flask import Blueprint, request, jsonify, render_template
from flask_weasyprint import HTML, render_pdf
from api.core import db
from api.utils import get_part_by_id, build_part_properties, is_number,\
    check_addtl_info, get_items, get_item, delete_item, get_categories,\
    engineering_notation
from api.decorators import requires_login, requires_roles, requires_keys,\
    requires_debug
from api.models.parts import Part, IC, Manufacturer, ChipType,\
    PartProperty, Unit, Kit

blueprint = Blueprint('parts', __name__, url_prefix='/parts')

FUZZ_THRESHOLD = 65


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('parts', Part, request))


@blueprint.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'part', Part))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('parts_manager')
def delete_part(id):
    return jsonify(delete_item(id, Part))


# We still don't have a better method for this!
@blueprint.route('/category/names/', methods=['GET'])
def get_part_cats():
    return jsonify(success=True, cats=get_categories())


def frexp10(x):
    exp = int(math.log10(x))
    return x / 10**exp, exp


def part_num_search(n, category):
    tmp = PartProperty.query.join(Part)\
        .filter(PartProperty.primary is True)\
        .filter(PartProperty.value is not None)\
        .filter(Part.category == category)\
        .all()
    tmp = [prop for prop in tmp if
           math.fabs(n - prop.value) / prop.value < 0.1 or
           frexp10(n)[0] == frexp10(prop.value)[0]]
    return [Part.query.get(prop.part_id).serialize for prop in tmp]


def part_attr_search(s, category):
    tmp = process.extract(s, [i.name for i in Unit.query.all()])
    tmp = [Unit.query.join(PartProperty).join(Part)
           .filter(Part.category == category)
           .filter(Unit.name == pair[0]).first() for
           pair in tmp if pair[1] > FUZZ_THRESHOLD]
    return [Part.query.join(PartProperty)
            .filter(PartProperty.unit == u).first().serialize for
            u in tmp if u]


def ic_attr_search(s, model, attr):
    tmp = process.extract(s, [getattr(i, attr) for i in model.query.all()])
    return [model.query.filter_by(**{attr: pair[0]}).first().serialize for
            pair in tmp if pair[1] > FUZZ_THRESHOLD]


@blueprint.route('/search/', methods=['POST'])
@requires_keys('category', 'query')
def search():
    errors = []
    json = request.get_json(force=True)
    parts = []

    if not json['category'] in get_categories() or not json['query']:
        errors.append('Bad Query!')

    if not errors and json['category'] == 'IC':
        parts += ic_attr_search(json['query'], IC, 'mftr_part_no')
        parts += ic_attr_search(json['query'], IC, 'name')
        parts += ic_attr_search(json['query'], IC, 'addtlInfo')
    elif not errors:
        parts += part_attr_search(json['query'], json['category'])
        n = re.sub('[^0-9]', '', json['query'])
        n = float(n) if is_number(n) else None
        if n:
            parts += part_num_search(n, json['category'])

    return jsonify(success=not errors, errors=errors, parts=parts)


@requires_keys('name', 'mftr', 'part_no', 'chip_type', 'location', 'stock')
def new_ic():
    errors = []
    json = request.get_json(force=True)
    part_id = None
    mftr = Manufacturer.query.filter_by(name=json['mftr']).first()
    chip_type = ChipType.query.filter_by(name=json['chip_type']).first()

    if not mftr:
        errors.append('Manufacturer does not exist')

    if not chip_type:
        errors.append('Chip Type does not exist')

    if not errors:
        ic = IC(
            name=json['name'],
            stock=json['stock'],
            price=json['price'],
            location=json['location'],
            mftr_id=mftr.id,
            mftr_part_no=json['part_no'],
            chip_type_id=chip_type.id,
            addtlInfo=json['main_info']['name']
        )
        db.session.add(ic)
        db.session.commit()
        part_id = ic.id + IC.IC_START_ID
        part = Part(category='IC', price=ic.price, id=part_id)
        ic.part_id = part_id
        db.session.add(part)
        db.session.commit()
    return jsonify(success=not errors, errors=errors, id=part_id)


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('category', 'price', 'main_info')
def new():
    errors = []
    json = request.get_json(force=True)
    part_id = None

    if not is_number(json['price']) or int(json['price']) <= 0:
        errors.append('Price must be a positive number.')

    if not json['category'] or json['category'] == 'Choose...':
        errors.append('Invalid category.')

    if not errors and json['category'] == 'IC':
        return new_ic()
    elif not errors:
        if not check_addtl_info([json['main_info']]):
            errors.append('Bad main_info structure')
        elif 'addtl_info' in json.keys() and \
                not check_addtl_info(json['addtl_info']):
            errors.append('Bad addtl_info structure')

        if not errors:
            part = Part(json['category'], json['price'])
            db.session.add(part)
            db.session.commit()
            part_id = part.id

            build_part_properties(part, [json['main_info']], True)
            if 'addtl_info' in json.keys():
                build_part_properties(part, json['addtl_info'])
            db.session.commit()

    return jsonify(success=not errors, errors=errors, id=part_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count', 'type')
def fake():
    json = request.get_json(force=True)
    n = json['count']

    if json['type'] == 'IC':
        ics = [api.fake.ic() for _ in xrange(n)]
        return jsonify(success=True, errors=[],
                       ids=[i.id + IC.IC_START_ID for i in ics])
    else:
        parts = [api.fake.static_part(json['type']) for _ in xrange(n)]
        return jsonify(success=True, errors=[],
                       ids=[i.id for i in parts])


@blueprint.route('/init/', methods=['POST'])
@requires_debug
def init():
    for _ in xrange(50):
        api.fake.static_part('RESISTOR')
    for _ in xrange(30):
        api.fake.static_part('CAPACITOR')
    for _ in xrange(5):
        api.fake.static_part('DIODE')
    for _ in xrange(5):
        api.fake.static_part('POT')
    for _ in xrange(50):
        api.fake.ic()
    return jsonify(success=True, errors=[])


@requires_keys('name', 'mftr', 'part_no', 'chip_type', 'location', 'stock')
def edit_ic(id):
    errors = []
    json = request.get_json(force=True)
    ic = IC.query.get(id - IC.IC_START_ID)

    ic.name = json['name']
    ic.stock = json['stock']
    ic.price = json['price']
    ic.location = json['location']
    ic.mftr_id = Manufacturer.query.filter_by(name=json['mftr']).first().id
    ic.mftr_part_no = json['part_no']
    ic.chip_type_id = ChipType.query.\
        filter_by(name=json['chip_type']).first().id
    ic.addtlInfo = json['main_info']['name']
    ic.part.price = json['price']
    db.session.add(ic)
    db.session.commit()

    return jsonify(success=not errors, errors=errors, id=id)


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('category', 'price', 'main_info')
def edit_part(id):
    errors = []
    json = request.get_json(force=True)
    part = get_part_by_id(id)

    if part is None:
        errors.append('Does not exist.')

    if not is_number(json['price']) or int(json['price']) <= 0:
        errors.append('Price must be a positive number.')

    if not errors and json['category'] == 'IC':
        return edit_ic(id)
    elif not errors:
        part.category = json['category']
        part.price = json['price']

        # db.session.delete(part.properties)
        PartProperty.query.filter_by(part_id=part.id).delete()
        db.session.add(part)
        db.session.commit()

        build_part_properties(part, [json['main_info']], True)
        if 'addtl_info' in json.keys():
            build_part_properties(part, json['addtl_info'])
        db.session.commit()

    return jsonify(success=not errors, errors=errors, id=id)


@blueprint.route('/catalog.pdf')
@requires_login
@requires_roles('officer')
def catalog():
    cats = [i for i in get_categories() if i !=
            'IC' and i != 'RESISTOR' and i != 'CAPACITOR']
    parts = {}

    # Sort resistors and capacitors by their primary value e.g. Ohm or F
    parts['RESISTOR'] = [i.serialize for i in
                         Part.query.join(PartProperty)
                         .filter(Part.category == 'RESISTOR')
                         .filter(PartProperty.primary)
                         .order_by(PartProperty.value)]

    parts['CAPACITOR'] = [i.serialize for i in
                          Part.query.join(PartProperty)
                          .filter(Part.category == 'CAPACITOR')
                          .filter(PartProperty.primary)
                          .order_by(PartProperty.value)]

    for cat in cats:
        parts[cat] = [i.serialize for i in
                      Part.query.filter_by(category=cat)
                      .order_by(Part.id).all()]

    return render_pdf(HTML(string=render_template(
        'catalog.html',
        parts=parts,
        ics=[i.serialize for i in IC.query.order_by(IC.id).all()],
        kits=[i.serialize for i in Kit.query.order_by(Kit.id).all()],
        engineering_notation=engineering_notation
    )))
