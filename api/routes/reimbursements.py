import api.fake
import datetime
from random import choice
from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from api.core import db
from api.models.finance import Reimbursement, Transaction, BudgetCategory
from api.models.users import User
from api.utils import get_items, delete_item, get_item, get_officers
from api.decorators import requires_login, requires_keys, requires_permissions,\
    requires_debug

blueprint = Blueprint('reimbursements', __name__, url_prefix='/reimbursements')
STATUSES = ['PENDING', 'APPROVED', 'COMPLETED']
TYPES = ['REGULAR', 'CASHBOX']


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_permissions('reimbursements', 'r')
def get_all():
    return jsonify(get_items('reimbursements', Reimbursement, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_permissions('reimbursements', 'w')
def delete(id):
        return jsonify(delete_item(id, Reimbursement))


@blueprint.route('/<int:id>/', methods=['GET'])
@requires_login
@requires_permissions('reimbursements', 'r')
def get(id):
    return jsonify(get_item(id, 'reimbursement', Reimbursement))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_permissions('reimbursements', 'w')
@requires_keys('amount', 'type', 'comment')
def new_reimbursement():
    errors = []
    json = request.get_json(force=True)
    request_id = None

    if not json['amount'] or int(json['amount']) <= 0:
        errors.append('Amount must be a positive value. ' +
                      'Include both dollars and cents.')

    if json['type'] not in TYPES:
        errors.append('Invalid type.')

    if not json['comment'].strip():
        errors.append('Comment cannot be an empty string')

    if not errors:
        reimbursement = Reimbursement(
            requester_id=current_user.id,
            amount=float(json['amount']) / 100,
            type=json['type'],
            comment=json['comment'],
            date_requested=datetime.datetime.now()
        )

        db.session.add(reimbursement)
        db.session.commit()
        request_id = reimbursement.id

    return jsonify(success=not errors, errors=errors, id=request_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    user = User.query.get(json['user_id']) if 'user_id' in json.keys()\
        else None
    type = json['type'] if 'type' in json.keys() else None
    reimbursements = [api.fake.reimbursement(user, type)
                      for _ in xrange(json['count'])]
    return jsonify(success=True, errors=[], ids=[i.id for i in reimbursements])


@blueprint.route('/resolve_all/', methods=['POST'])
@requires_debug
def resolve_reimbursements():
    budget_category = BudgetCategory.query\
        .filter_by(name='Reimbursements').first()
    incomplete = Reimbursement.query\
        .filter(Reimbursement.status != 'COMPLETED')\
        .filter(Reimbursement.type == 'REGULAR')

    for burse in incomplete:
        burse.status = 'COMPLETED'
        transaction = Transaction(
            user_id=choice(get_officers()).id,
            budget_category=budget_category,
            amount=-burse.amount,
            date=datetime.datetime.now()
        )
        db.session.add(burse)
        db.session.add(transaction)
        db.session.commit()

    return jsonify(success=True, errors=[])


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_permissions('reimbursements', 'w')
@requires_keys('amount', 'type', 'comment', 'status')
def edit(id):
    errors = []
    json = request.get_json(force=True)
    reimbursement = Reimbursement.query.get(id)

    if not reimbursement:
        errors.append('Does not exist.')

    if not errors and json['status'] not in STATUSES:
        errors.append('Invalid status.')

    if not errors and json['type'] not in TYPES:
        errors.append('Invalid type.')

    if not errors:
        reimbursement.amount = float(json['amount']) / 100
        reimbursement.comment = json['comment']
        reimbursement.type = json['type']
        reimbursement.status = json['status']
        if json['status'] == 'APPROVED':
            reimbursement.date_approved = datetime.datetime.now()
        if json['status'] == 'COMPLETED' and json['type'] == 'REGULAR':
            budget_category = BudgetCategory.query\
                .filter_by(name='Reimbursements').first()
            transaction = Transaction(
                user_id=current_user.id,
                budget_category=budget_category,
                amount=-float(json['amount']) / 100,
                date=datetime.datetime.now()
            )
            db.session.add(transaction)
        db.session.commit()
    return jsonify(success=not errors, errors=errors)
