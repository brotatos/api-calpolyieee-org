from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.posts import ImageLink

blueprint = Blueprint('imagelinks', __name__, url_prefix='/imagelinks')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def get_all():
    return jsonify(get_items('imagelinks', ImageLink, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('poster')
def delete(id):
    return jsonify(delete_item(id, ImageLink))
