import datetime
import api.fake
from flask import Blueprint, request, jsonify
from api.core import db
from api.models.finance import CashboxTotal
from api.utils import get_items, delete_item
from api.decorators import requires_login, requires_keys,\
    requires_permissions, requires_debug

blueprint = Blueprint('cashbox_totals', __name__,
                      url_prefix='/cashbox_totals')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_permissions('cashbox', 'r')
def get_all():
    return jsonify(get_items('cashbox_totals', CashboxTotal, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_permissions('cashbox', 'w')
def delete(id):
    return jsonify(delete_item(id, CashboxTotal))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_permissions('cashbox', 'w')
@requires_keys('amount')
def new():
    json = request.get_json(force=True)
    errors = []
    total_id = None

    if not json['amount']:
        errors.append('Invalid amount.')

    if not errors:
        total = CashboxTotal(
            amount=float(json['amount']) / 100,
            date=datetime.datetime.now()
        )
        db.session.add(total)
        db.session.commit()
        total_id = total.id

    return jsonify(success=not errors, errors=errors, id=total_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    totals = [api.fake.cashbox_total() for _ in xrange(json['count'])]
    return jsonify(success=True, errors=[], ids=[i.id for i in totals])
