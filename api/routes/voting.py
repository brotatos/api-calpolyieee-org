import datetime
import math
import api.fake
from random import choice
from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from api.core import db
from api.models.users import User, Role
from api.models.core import Config
from api.models.voting import Position, Vote, Answer, Question
from api.utils import unix_time
from api.decorators import requires_login, requires_roles, requires_keys,\
    requires_debug

blueprint = Blueprint('voting', __name__, url_prefix='/voting')


def verify_ballot(ballot):
    """
    Verifies all information related to ballot json by checking against the db.
    """
    errors = []
    success = True

    for position, picks in ballot.iteritems():
        dbPos = Position.query.filter_by(name=position).first()
        if not dbPos:
            success = False
            errors.append('Position ' + position + 'does not exist')
        elif len(picks) != dbPos.multiple:
            success = False
            errors.append('You must select ' + str(dbPos.multiple) +
                          ' candidates for the position ' + position)
        else:
            for pick in picks:
                dbUser = User.query.filter_by(name=choice).first()

                if '[WRITE IN]' not in choice and not dbUser:
                    success = False
                    errors.append('Candidate' + pick + 'does not exist')

    return {'success': success, 'errors': errors}


@blueprint.route('/status/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def get_voting_status():
    '''
    Determine if voting is going on.
    '''
    config = Config.query.first()
    return jsonify(time_remaining=unix_time(config.voting_end_date)
                   - unix_time(), started=config.voting_started)


def _clear_voting():
    Vote.query.delete()
    Answer.query.delete()
    db.engine.execute('DELETE FROM candidate')
    db.engine.execute('UPDATE users_user SET voted = false')
    db.session.commit()


@blueprint.route('/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def clear_voting():
    _clear_voting()
    return jsonify(success=True)


@blueprint.route('/process/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def process_voting():
    """
    Count up the votes from the ballot.
    """
    write_ins = []
    results = {}

    positions = Position.query.all()
    for position in positions:
        candidates = {}
        for candidate in position.candidates:
            candidates[candidate.name] = 0
        results[position.name] = candidates

    for position in positions:
        votes = Vote.query.filter_by(position_id=position.id).all()
        for vote in votes:
            user = User.query.filter_by(id=vote.candidate_id).first()
            if user.name == 'Write In':
                write_ins.append(position.name + ' : ' + vote.write_in)
            else:
                results[position.name][user.name] += 1

    return jsonify(success=True, results=results, write_ins=write_ins)


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('voter')
def get_ballot():
    """
    Get the current voting ballot to choose which candidates for which
    position.
    """
    now = datetime.datetime.now()
    config = Config.query.first()

    if now < config.voting_end_date and not config.voting_started:
        return jsonify(success=False,
                       errors=['The voting period has not opened yet'])

    if now < config.voting_end_date and current_user.voted:
        return jsonify(success=False,
                       errors=['You have already voted. Thank you!'])

    if now > config.voting_end_date:
        return jsonify(success=False,
                       errors=['The voting period has ended'])

    ballot = {
        'time_remaining': unix_time(config.voting_end_date) - unix_time(),
        'positions': [i.serialize for i in Position.query.all()]
    }
    return jsonify(success=True, ballot=ballot)


# takes in a JSON ballot and checks it for errors
@blueprint.route('/verify/', methods=['POST'])
@requires_login
@requires_roles('voter')
@requires_keys('ballot')
def run_verify_ballot():
    return jsonify(verify_ballot(request.get_json(force=True)['ballot']))


# Submitted ballots will look like this
# ballot = {
#     'president': ['candidate'],
#     'position where two people can run': ['candidate 1', 'candidate 2']
# }
@blueprint.route('/submit/', methods=['POST'])
@requires_login
@requires_roles('voter')
@requires_keys('ballot')
def submit_ballot():
    json = request.get_json(force=True)
    verify = verify_ballot(json['ballot'])

    if not verify['success']:
        return jsonify(success=False, errors=['Ballot failed verification.'])

    if current_user.voted:
        return jsonify(success=False, errors=['Forbidden.'])

    for position, picks in json['ballot'].iteritems():
        db_position = Position.query.filter_by(name=position).first()
        for pick in picks:
            if '[WRITE IN]' in pick:
                can_id = User.query.filter_by(name='WRITE_IN').first().id
                write_in = pick
            else:
                can_id = User.query.filter_by(name=pick).first().id
                write_in = ''

            vote = Vote(voter_id=current_user.id,
                        position_id=db_position.id,
                        candidate_id=can_id,
                        write_in=write_in)
            db.session.add(vote)
            db.session.commit()

    current_user.voted = True
    db.session.commit()

    return jsonify(success=True)


@blueprint.route('/fake_candidates/', methods=['POST'])
@requires_debug
def fake_candidates():
    CDT_PER_POS = 5.0
    req_cdts = int(math.ceil(float(len(Position.query.all())) *
                             CDT_PER_POS / 2.0))
    cdt_roles = [Role.query.filter_by(name='delete_eligible').first(),
                 Role.query.filter_by(name='voter').first()]
    candidates = [api.fake.user(cdt_roles) for _ in xrange(req_cdts)]

    _clear_voting()

    for cdt in candidates:
        cdt.positions = []
        api.fake.fill_user_profile(cdt)

    db.session.commit()

    for position in Position.query.all():
        position.candidates = []
        for _ in xrange(int(CDT_PER_POS)):
            cdt = choice(candidates)
            while len(cdt.positions) > 1 or cdt in position.candidates:
                cdt = choice(candidates)

            position.candidates.append(cdt)
            cdt.positions.append(position)

            for question in Question.query.all():
                api.fake.answer(question, cdt, position)

    db.session.commit()

    return jsonify(success=True, errors=[])


@blueprint.route('/fake_election/', methods=['POST'])
@requires_debug
def fake_election():
    positions = Position.query.all()
    voters = [user for user in User.query.all() if 'voter' in
              [role.serialize for role in user.roles]]

    for voter in voters:
        for position in positions:
            picks = []
            for _ in xrange(position.multiple):
                pick = choice(position.candidates)
                while pick in picks:
                    pick = choice(position.candidates)
                picks.append(pick)

            for pick in picks:
                vote = Vote(voter_id=voter.id,
                            position_id=position.id,
                            candidate_id=pick.id)
                db.session.add(vote)
        voter.voted = True
    db.session.commit()

    return jsonify(success=True, errors=[])
