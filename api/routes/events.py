from flask import Blueprint, request, jsonify
from api.core import db
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.events import Event
# from calpoly.twitterBot import tweet

blueprint = Blueprint('events', __name__, url_prefix='/events')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('events', Event, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('publicity')
def delete(id):
    return jsonify(delete_item(id, Event))


# TODO: Add tweeting functionality back in here.
@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('publicity')
@requires_keys('event', 'options')
def new():
    errors = []
    json = request.get_json(force=True)
    event_id = None

    if json['event'] == "":
        errors.append("Empty event not allowed")

    if not errors:
        # if json['options']['twitter']:
        #     tweet(json['event'], errors=errors)

        if not errors:
            event = Event(json['event'])
            db.session.add(event)
            db.session.commit()
            event_id = event.id

    return jsonify(success=not errors, errors=errors, id=event_id)
