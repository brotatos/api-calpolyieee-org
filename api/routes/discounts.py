from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items, get_item
from api.decorators import requires_login, requires_roles
from api.models.finance import Discount

blueprint = Blueprint('discounts', __name__, url_prefix='/discounts')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('parts_seller')
def get_all():
    return jsonify(get_items('discounts', Discount, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Discount))


@blueprint.route('/<int:id>/', methods=['GET'])
@requires_login
@requires_roles('parts_seller')
def get(id):
    return jsonify(get_item(id, 'discount', Discount))
