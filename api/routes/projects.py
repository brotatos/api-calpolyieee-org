import api.fake
from flask import Blueprint, request, jsonify
from api.core import db
from api.models.users import User
from api.models.posts import Tag
from api.models.projects import Project
from api.utils import delete_item, get_items, has_content, sanitize,\
    get_or_create, get_item, get_officers
from api.decorators import requires_login, requires_roles, requires_keys,\
    requires_debug

blueprint = Blueprint('projects', __name__, url_prefix='/projects')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('projects', Project, request))


@blueprint.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'project', Project))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Project))


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_roles('officer')
@requires_keys('title', 'completed', 'repo', 'contact', 'image', 'caption',
               'description', 'content')
def edit_project(id):
    json = request.get_json(force=True)
    errors = []
    project = Project.query.get(id)
    contact = User.query.filter_by(name=json['contact']).first()
    prj_url = json['title'].replace(' ', '_').lower()
    prj_tag = get_or_create(prj_url, Tag, name=prj_url)

    if project is None:
        errors.append('Does not exist.')

    if contact is None:
        errors.append('Project contact does not exist.')

    if prj_url == '':
        errors.append('Invalid project title.')

    if not has_content(json['content']):
        errors.append('Project must have content.')

    if not errors:
        project.title = json['title']
        project.completed = json['completed']
        project.repo = json['repo']
        project.project_contact = contact
        project.image_link = json['image']
        project.image_caption = json['caption']
        project.description = json['description']
        project.content = sanitize(json['content'])
        project.url = prj_url
        project.associated_tag = prj_tag
        db.session.commit()

    return jsonify(success=not errors, errors=errors,
                   url=prj_url, tag=prj_tag.serialize)


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('officer')
@requires_keys('title', 'completed', 'repo', 'contact', 'image', 'caption',
               'description', 'content')
def new_project():
    json = request.get_json(force=True)
    errors = []
    contact = User.query.filter_by(name=json['contact']).first()
    prj_url = json['title'].replace(' ', '_').lower()
    prj_tag = get_or_create(prj_url, Tag, name=prj_url)

    if contact is None:
        errors.append('Project contact does not exist.')

    if prj_url == '':
        errors.append('Invalid project title.')

    if Project.query.filter_by(url=prj_url).first():
        errors.append('A project already exists with that title')

    if not has_content(json['content']):
        errors.append('Project must have content.')

    if not errors:
        project = Project(
            title=json['title'],
            completed=json['completed'],
            repo=json['repo'],
            contact=contact,
            image=json['image'],
            caption=json['caption'],
            description=json['description'],
            content=sanitize(json['content']),
            url=prj_url,
            tag=prj_tag
        )
        db.session.add(project)
        db.session.commit()

    return jsonify(success=not errors, errors=errors,
                   url=prj_url, tag=prj_tag.serialize)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    contact = None
    is_completed = json['type'] if 'type' in json.keys() else None
    errors = []
    ids = []

    if 'contact' in json.keys():
        contact = User.query.filter_by(name=json['contact']).first()
        if contact not in get_officers():
            errors.append('Project contact must be an officer.')

    if not errors:
        projects = [api.fake.project(contact, is_completed)
                    for _ in xrange(json['count'])]
        ids = [i.id for i in projects]

    return jsonify(success=not errors, errors=errors, ids=ids)
