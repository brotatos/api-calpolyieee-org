from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from api.core import db
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.voting import Answer, Question, Position

blueprint = Blueprint('answers', __name__, url_prefix='/answers')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def get_all():
    return jsonify(get_items('answers', Answer, request))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_keys('question_id', 'position_id', 'response')
def new():
    json = request.get_json(force=True)
    errors = []
    question = Question.query.get(json['question_id'])
    position = Position.query.get(json['position_id'])

    if question is None:
        errors.append('Question does not exist.')

    if position is None:
        errors.append('Position does not exist.')

    if not errors:
        answer = Answer(question_id=question.id, user_id=current_user.id,
                        position_id=position.id, answer=json['response'])
        db.session.add(answer)
        db.session.commit()

    return jsonify(success=not errors, errors=errors)


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Answer))
