from flask import Blueprint, request, jsonify
from sqlalchemy.exc import IntegrityError
from api.core import db
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.parts import Manufacturer

blueprint = Blueprint('manufacturers', __name__, url_prefix='/manufacturers')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('manufacturers', Manufacturer, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Manufacturer))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('name')
def add_new_manufacturer():
    errors = []
    json = request.get_json(force=True)

    try:
        db.session.add(Manufacturer(json['name']))
        db.session.commit()
    except IntegrityError:
        errors.append('Already exists')
    return jsonify(success=not errors, errors=errors)
