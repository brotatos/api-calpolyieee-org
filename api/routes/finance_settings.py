from flask import Blueprint, request, jsonify
from api.core import db
from api.models.finance import FinanceSettings
from api.models.users import User
from api.utils import get_items, delete_item
from api.decorators import requires_login, requires_keys, requires_permissions

blueprint = Blueprint('settings', __name__, url_prefix='/settings')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_permissions('settings', 'rw')
def get_all():
    return jsonify(get_items('settings', FinanceSettings, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_permissions('settings', 'w')
def delete(id):
        return jsonify(delete_item(id, FinanceSettings))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_permissions('settings', 'w')
@requires_keys('user_id', 'permissions')
def new_user():
    errors = []
    json = request.get_json(force=True)
    user = User.query.get(json['user_id'])
    finance_settings_id = None

    if not user:
        errors.append('User does not exist.')

    if not json['permissions']:
        errors.append('Invalid permissions string.')

    if not errors:
        finance_settings = FinanceSettings(
            user=user,
            permissions=json['permissions']
        )

        db.session.add(finance_settings)
        db.session.commit()
        finance_settings_id = finance_settings.id

    return jsonify(success=not errors, errors=errors,
                   id=finance_settings_id)


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_permissions('settings', 'w')
@requires_keys('permissions')
def edit(id):
    errors = []
    json = request.get_json(force=True)
    finance_settings = FinanceSettings.query.get(id)

    if not finance_settings:
        errors.append('Not Found.')

    if not json['permissions']:
        errors.append('Invalid permissions string.')

    if not errors:
        finance_settings.permissions = json['permissions']
        db.session.commit()

    return jsonify(success=not errors, errors=errors)
