from flask import Blueprint, request, jsonify
from sqlalchemy.exc import IntegrityError
from api.core import db
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.parts import ChipType

blueprint = Blueprint('chip_types', __name__, url_prefix='/chip_types')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('chip_types', ChipType, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('parts_manager')
def delete(id):
    return jsonify(delete_item(id, ChipType))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('name')
def new():
    json = request.get_json(force=True)
    chipType = ChipType(json['name'])
    try:
        db.session.add(chipType)
        db.session.commit()
    except IntegrityError:
        return jsonify(success=False, errors=['Already exists'])
    return jsonify(success=True)
