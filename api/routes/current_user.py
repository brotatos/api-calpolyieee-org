from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from werkzeug import check_password_hash, generate_password_hash
from api.core import db
from api.decorators import requires_login, requires_keys, requires_roles
from api.models.finance import FinanceSettings
from api.models.users import Subject
from api.models.voting import Question, Answer, Position

blueprint = Blueprint('current_user', __name__, url_prefix='/current_user')


@blueprint.route('/', methods=['GET'])
@requires_login
def get():
    user = current_user.serialize
    perms = FinanceSettings.query.filter_by(user=current_user).first()

    if perms:
        user['permissions'] = perms.permissions
    return jsonify(success=True, user=user)


@blueprint.route('/change_password/', methods=['POST'])
@requires_login
@requires_keys('current', 'new', 'confirm')
def change_password():
    errors = []
    json = request.get_json(force=True)

    if json['new'] != json['confirm']:
        errors.append('Passwords do not match.')

    if len(json['new']) < 6:
        errors.append('Password is too short.')

    if not check_password_hash(current_user.password, json['current']):
        errors.append('Invalid current password.')

    if not errors:
        current_user.password = generate_password_hash(json['new'])
        db.session.commit()

    return jsonify(success=not errors, errors=errors)


@blueprint.route('/', methods=['PUT'])
@requires_login
@requires_roles('officer')
@requires_keys('year', 'major', 'fb_id', 'bio', 'classes', 'name')
def edit_profile():
    json = request.get_json(force=True)
    errors = []

    current_user.classes = [Subject.query.get(i['id'])
                            for i in json['classes']]
    current_user.bio = json['bio']
    current_user.year = json['year']
    current_user.major = json['major']
    current_user.facebookid = json['fb_id']
    current_user.name = json['name']

    db.session.commit()
    return jsonify(success=not errors, errors=errors)


# Run for a position, for officer elections
@blueprint.route('/run/', methods=['POST'])
@requires_login
@requires_keys('title', 'answers', 'name', 'year')
def run():
    errors = []
    json = request.get_json(force=True)
    position = Position.query.filter_by(name=json['title']).first()

    if position is None:
        errors.append('Position does not exist.')

    if not errors:
        for j_ans in json['answers']:
            question = Question.query.filter_by(prompt=j_ans['prompt']).first()

            if question is None or 'response' not in j_ans.keys() \
                    or not j_ans['response']:
                errors.append('Invalid response.')
                continue

            answer = Answer(question_id=question.id,
                            user_id=current_user.id,
                            position_id=position.id,
                            answer=j_ans['response'])
            db.session.add(answer)

    if not errors:
        current_user.name = json['name']
        current_user.year = json['year']

    if len(current_user.positions) >= 2:
        errors.append('You cannot run for more than 2 positions.')

    if json['title'] == 'President' and \
            'officer' not in current_user.get_roles():
        errors.append("You must be a current officer to run for President")

    if not errors:
        current_user.positions.append(position)
        db.session.commit()
    return jsonify(success=not errors, errors=errors)
