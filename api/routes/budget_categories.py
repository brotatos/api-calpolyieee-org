from flask import Blueprint, request, jsonify
from api.core import db
from api.models.finance import BudgetCategory, Transaction
from api.utils import get_items, get_item
from api.decorators import requires_login, requires_keys

blueprint = Blueprint('budget_categories', __name__,
                      url_prefix='/budget_categories')


@blueprint.route('/', methods=['GET'])
@requires_login
def get_all():
    return jsonify(get_items('budget_categories', BudgetCategory, request))


@blueprint.route('/<int:id>/', methods=['GET'])
@requires_login
def get(id):
    return jsonify(get_item(id, 'budget_category', BudgetCategory))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
def delete(id):
    bc = BudgetCategory.query.get(id)
    errors = []

    if not bc:
        errors.append('Category does not exist.')

    if not errors:
        transactions = Transaction.query.filter_by(budget_category=bc).all()
        if transactions:
            errors.append('Cannot delete non-empty category.')
        else:
            db.session.delete(bc)
            db.session.commit()

    return jsonify(success=not errors, errors=errors)


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_keys('name', 'type')
def new_category():
    json = request.get_json(force=True)
    errors = []
    category_id = None

    if not json['name']:
        errors.append('Category name cannot be blank.')

    if json['type'] != 'TRACKING' and json['type'] != 'NON_TRACKING':
        errors.append('Invalid category type.')

    if not errors:
        category = BudgetCategory(name=json['name'], type=json['type'])

        db.session.add(category)
        db.session.commit()
        category_id = category.id

    return jsonify(success=not errors, errors=errors, id=category_id)
