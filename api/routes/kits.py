import api.fake
from flask import Blueprint, request, jsonify
from api.core import db
from api.models.parts import Kit, KitItem
from api.utils import get_items, delete_item, is_number, get_item
from api.decorators import requires_login, requires_keys, requires_roles,\
    requires_debug

blueprint = Blueprint('kits', __name__, url_prefix='/kits')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('kits', Kit, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('parts_manager')
def delete(id):
    return jsonify(delete_item(id, Kit))


@blueprint.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'kit', Kit))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('price', 'name', 'kit_items', 'location', 'stock')
def new():
    errors = []
    json = request.get_json(force=True)
    part_id = None

    if json['price'] is None or not is_number(json['price']):
        errors.append('Price must be a number.')

    if json['stock'] is None or not is_number(json['stock']):
        errors.append('Stock must be a number.')

    if not json['name']:
        errors.append('Must provide a valid kit name.')

    if not len(json['kit_items']):
        errors.append('All kits must have at least one item')

    if not errors:
        kit = Kit(
            name=json['name'],
            stock=json['stock'],
            price=json['price'],
            location=json['location'],
            kit_items=[]
        )
        db.session.add(kit)
        db.session.commit()
        kit.kit_items = [KitItem(kit_id=kit.id,
                                 part_id=item['id'],
                                 quantity=item['quantity'])
                         for item in json['kit_items']]
        db.session.commit()
        part_id = kit.id
    return jsonify(success=not errors, errors=errors, id=part_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    title = json['title'] if 'title' in json.keys() else None
    kits = [api.fake.kit(title) for _ in xrange(json['count'])]
    return jsonify(success=True, errors=[], ids=[i.id for i in kits])


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_roles('parts_manager')
@requires_keys('price', 'name', 'kit_items', 'location', 'stock')
def edit_kit(id):
    '''
    Edit a kit.
    '''
    errors = []
    json = request.get_json()
    kit = Kit.query.get(id)

    if kit is None:
        errors.append('Kit does not exist')

    if not errors:
        kit.name = json['name']
        kit.stock = json['stock']
        kit.price = json['price']
        kit.location = json['location']
        kit.kit_items = [KitItem(kit_id=kit.id,
                                 part_id=item['id'],
                                 quantity=item['quantity'])
                         for item in json['kit_items']]

        db.session.add(kit)
        db.session.commit()
    return jsonify(success=not errors, errors=errors, id=id)
