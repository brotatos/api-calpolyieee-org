import api.fake
from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from api.utils import get_or_create, delete_item, get_item, get_items,\
    sanitize, has_content
from api.core import db
from api.models.posts import Post, Tag, ImageLink
from api.models.users import User
from api.decorators import requires_login, requires_roles, requires_keys,\
    requires_debug

blueprint = Blueprint('posts', __name__, url_prefix='/posts')


@blueprint.route('/', methods=['GET'])
def get_container():
    return jsonify(get_items('posts', Post, request))


@blueprint.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'post', Post))


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_roles('poster')
@requires_keys('title', 'content', 'tags', 'images')
def edit_post(id):
    errors = []
    json = request.get_json(force=True)
    post = Post.query.get(id)

    if not post:
        errors.append("Post does not exist.")
    elif post.user_id != current_user.id:
        errors.append("Forbidden.")

    if not errors:
        post.title = json['title']
        post.content = sanitize(json['content'])
        post.tags = [get_or_create(tag['name'], Tag, name=tag['name'])
                     for tag in json['tags']]
        post.images = [get_or_create(link, ImageLink, link=link)
                       for link in json['images']]
        db.session.commit()
    return jsonify(success=not errors, errors=errors)


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('poster')
def delete(id):
    return jsonify(delete_item(id, Post, user=current_user))


# This can be removed soon as get_items now provides the
# same functionality
@blueprint.route('/recent/<int:max_id>/', methods=['GET'])
def get_last_ten_posts(max_id=None):
    # The minimum id: grab the last ten posts so substract it from the max_id
    return jsonify(posts=[i.serialize for i in Post.query
                          .order_by(Post.date.desc())
                          .filter(Post.id >= max_id - 10, Post.id < max_id,
                                  Post.sticky == 'f')])


# NOTE
# /posts/recent/ can be replaced with the following url:
# /posts?sort=-date&limit=10&filter=sticky==false


# No way to do this with get_items right now
@blueprint.route('/with_tag/<string:tag>/', methods=['GET'])
def get_posts_by_tag(tag):
    return jsonify(posts=[i.serialize for i in Post.query
                          .order_by(Post.date.desc())
                          .filter(Post.tags.any(name=tag))])


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('poster')
@requires_keys('title', 'content', 'tags', 'images')
def new():
    errors = []
    json = request.get_json(force=True)
    post = Post(json, current_user.id)

    if not has_content(post.content):
        errors.append('Can\'t create empty post.')

    if not errors:
        db.session.add(post)
        db.session.commit()
    return jsonify(success=not errors, errors=errors)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    user = User.query.get(json['user_id']) if 'user_id' in json.keys()\
        else None
    posts = [api.fake.post(user) for _ in xrange(json['count'])]
    return jsonify(success=True, errors=[], ids=[i.id for i in posts])
