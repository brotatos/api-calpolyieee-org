from flask import Blueprint, request, jsonify
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.core import Config

blueprint = Blueprint('config', __name__, url_prefix='/config')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def get():
    return jsonify(success=True, config=Config.query.first().serialize)


@blueprint.route('/', methods=['PUT'])
@requires_login
@requires_roles('webmaster')
@requires_keys('voting_started', 'lounge_open', 'voting_end_date',
               'landing_image', 'officers_image')
def edit():
    Config.query.first().edit(request.get_json(force=True))
    return jsonify(success=True)
