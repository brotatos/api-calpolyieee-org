from flask import Blueprint, request, jsonify
from api.core import db
from api.utils import delete_item, DAY_END, get_time_slots
from api.decorators import requires_login, requires_roles, requires_keys
from api.models.users import User, TimeSlot

blueprint = Blueprint('time_slots', __name__, url_prefix='/time_slots')


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, TimeSlot))


@blueprint.route('/', methods=['GET'])
def get_all_time_slots():
    return jsonify(success=True, schedule=get_time_slots())


# clear the time slot table
@blueprint.route('/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def clear_time_slots():
    TimeSlot.query.delete()
    db.session.commit()
    return jsonify(success=True)


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('secretary')
@requires_keys('user_id', 'hour', 'day_of_week')
def new_time_slot():
    json = request.get_json(force=True)
    errors = []
    days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY']
    user = User.query.get(json['user_id'])
    time_slot_id = None
    user_name = None
    user_year = None

    if user is None:
        errors.append('User does not exist.')

    if not json['day_of_week'] in days:
        errors.append('`day_of_week` must be a weekday.')

    if int(json['hour']) > DAY_END or int(json['hour']) < 0:
        errors.append('`hour` must be between 0 and 24.')

    if not errors:
        time_slot = TimeSlot(user_id=user.id,
                             hour=json['hour'],
                             day_of_week=json['day_of_week'])
        db.session.add(time_slot)
        db.session.commit()
        time_slot_id = time_slot.id
        user_name = user.name
        user_year = user.year

    return jsonify(success=not errors, errors=errors, id=time_slot_id,
                   user_name=user_name, user_year=user_year)
