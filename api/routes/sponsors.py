from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.sponsors import Sponsor

blueprint = Blueprint('sponsors', __name__, url_prefix='/sponsors')


@blueprint.route('/', methods=['GET'])
@requires_login
def get_all():
    return jsonify(get_items('sponsors', Sponsor, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Sponsor))
