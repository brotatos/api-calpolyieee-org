from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.posts import Tag

blueprint = Blueprint('tags', __name__, url_prefix='/tags')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('tags', Tag, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Tag))
