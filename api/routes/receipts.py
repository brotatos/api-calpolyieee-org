import datetime
import api.fake
from flask import Blueprint, request, jsonify, render_template
from fuzzywuzzy import process
from flask_weasyprint import HTML, render_pdf
from flask.ext.login import current_user
from api.core import db
from api.models.users import User
from api.models.finance import ReceiptItem, Receipt, Transaction,\
    BudgetCategory, Discount
from api.utils import delete_item, get_item, get_items,\
    get_part_by_id, engineering_notation
from api.decorators import requires_login, requires_roles, requires_keys,\
    requires_debug

blueprint = Blueprint('receipts', __name__, url_prefix='/receipts')

FUZZ_THRESHOLD = 65


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('treasurer')
def get_all():
    return jsonify(get_items('receipts', Receipt, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('treasurer')
def delete(id):
    return jsonify(delete_item(id, Receipt))


@blueprint.route('/<int:id>/', methods=['GET'])
@requires_login
@requires_roles('parts_seller')
def get(id):
    return jsonify(get_item(id, 'receipt', Receipt))


@blueprint.route('/search/<string:q>/', methods=['GET'])
@requires_login
@requires_roles('parts_seller')
def search(q):
    errors = []
    receipts = process.extract(q, [r.purchaser for r in Receipt.query.all()])
    receipts = [Receipt.query.filter_by(purchaser=pair[0]).first().serialize
                for pair in receipts
                if pair[1] > FUZZ_THRESHOLD]
    return jsonify(success=not errors, errors=errors, receipts=receipts)


@blueprint.route('/finalize/', methods=['POST'])
@requires_login
@requires_roles('parts_seller')
@requires_keys('items')
def finalize():
    json = request.get_json(force=True)
    errors = []

    if not len(json['items']):
        errors.append('Cannot create empty receipt')

    receipt = {
        'items': [i for i in json['items'] if i['category'] != 'DISCOUNT'],
        'total_price': 0
    }

    for item in receipt['items']:
        receipt['total_price'] += item['quantity'] * item['price']

    for item in [i for i in json['items'] if i['category'] == 'DISCOUNT']:
        discount = Discount.query.get(item['id'] - Discount.START_ID)
        if discount.amount:
            item['price'] = -1 * discount.amount * 100
        else:
            item['price'] = -1 * (discount.percent / 100) * \
                receipt['total_price']
        receipt['items'].append(item)

    # Recalculate the receipt's total price taking into account all discounts
    receipt['total_price'] = 0
    for item in receipt['items']:
        receipt['total_price'] += item['quantity'] * item['price']

    return jsonify(success=not errors, errors=errors, receipt=receipt)


# make a new receipt, will also make a new transaction
# allowed to all officers
@blueprint.route('/', methods=['POST'])
@requires_login
@requires_roles('parts_seller')
@requires_keys('total_price', 'items', 'purchaser', 'comment')
def new_receipt():
    json = request.get_json(force=True)
    receipt_id = None
    errors = []

    if not len(json['items']) and not json['comment']:
        errors.append('Receipt must contain at least 1 item')

    if int(json['total_price']) == 0:
        errors.append('total_price must be a non-zero amount in cents')

    for item in json['items']:
        if not isinstance(item, dict) or 'quantity' not in item.keys():
            errors.append('Bad item object structure')
        else:
            if not item['category'] == 'DISCOUNT' and \
               not item['category'] == 'MANUAL' and \
               not get_part_by_id(item['id']):
                errors.append('IEEE part id ' + str(item['id']) +
                              ' not found in database')

            if int(item['quantity']) == 0:
                errors.append('Quantity cannot be 0.')

    # this is where we actually construct the receipt and transaction
    if not errors:
        budget_category = BudgetCategory.query\
            .filter_by(name='Part Sales').first()
        transaction = Transaction(
            user_id=current_user.id,
            budget_category=budget_category,
            amount=float(json['total_price']) / 100,
            date=datetime.datetime.now()
        )

        db.session.add(transaction)
        db.session.commit()

        receipt = Receipt(
            seller_id=current_user.id,
            transaction_id=transaction.id,
            purchaser=json['purchaser'],
            comment=json['comment']
        )

        db.session.add(receipt)
        db.session.commit()
        receipt_id = receipt.id

        for item in [i for i in json['items'] if i['category'] != 'DISCOUNT'
                     and i['category'] != 'MANUAL']:
            receipt_item = ReceiptItem(
                receipt_id=receipt.id,
                ieee_part_no=item['id'],
                quantity=item['quantity']
            )
            part = get_part_by_id(item['id'])

            if 'stock' in part.__dict__.keys() and part.stock > 0:
                part.stock -= item['quantity']

            db.session.add(receipt_item)

        for item in [i for i in json['items'] if i['category'] == 'DISCOUNT']:
            discount = Discount.query.get(item['id'] - Discount.START_ID)

            receipt_item = ReceiptItem(
                receipt_id=receipt.id,
                ieee_part_no=item['id'],
                discount_id=discount.id,
                discount_amount=float(item['price']) / 100
            )
            db.session.add(receipt_item)

        manual_string = ''
        for item in [i for i in json['items'] if i['category'] == 'MANUAL']:
            manual_string += 'This receipt includes an item of price $' + \
                str(float(item['price']) / 100) + ' not in the database. '

        receipt.comment = manual_string + receipt.comment
        db.session.commit()

    return jsonify(success=not errors, errors=errors, receipt_id=receipt_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    user = User.query.get(json['user_id'])\
        if 'user_id' in json.keys() else None
    receipts = [api.fake.receipt(user) for _ in xrange(json['count'])]
    return jsonify(success=True, errors=[], ids=[i.id for i in receipts])


@blueprint.route('/pdf/<int:id>.pdf/', methods=['GET'])
@requires_login
@requires_roles('officer', 'parts_seller')
def get_receipt_pdf(id):
    receipt = Receipt.query.get_or_404(id)
    return render_pdf(HTML(string=render_template(
        'receipt.html',
        receipt=receipt,
        items=receipt.serialize['items'],
        engineering_notation=engineering_notation
    )))
