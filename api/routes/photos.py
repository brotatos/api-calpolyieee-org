from flask import Blueprint, request, jsonify
from api.core import db
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.photos import Photo
# from calpoly.twitterBot import upload_photo_to_twitter

blueprint = Blueprint('photos', __name__, url_prefix='/photos')


@blueprint.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('photos', Photo, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('publicity')
def delete(id):
    return jsonify(delete_item(id, Photo))


# TODO: Add tweeting functionality back in later.
@blueprint.route('/', methods=['PUT'])
@requires_login
@requires_roles('publicity')
def update_main_page_photos():
    response = {'success': False, 'twitter': []}
    json = request.get_json(force=True)
    # old_photos = Photo.query.all()
    photos = json['photos']

    # Delete all photo objects.
    Photo.query.delete()

    for photo in photos:
        # new = True
        # for old_photo in old_photos:
        #     if photo == old_photo.url:
        #         new = False
        # if new:
        #     upload_photo_to_twitter(photo, response['twitter'])
        db.session.add(Photo(photo))

    db.session.commit()
    response['success'] = True
    return jsonify(response)
