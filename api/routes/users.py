import api.fake
from flask import Blueprint, request, jsonify
from flask.ext.login import login_user, logout_user
from recaptcha.client.captcha import submit
from werkzeug import check_password_hash, generate_password_hash
from api.core import db, RECAPTCHA_PRIVATE_KEY
from api.models.users import Role, User, Subject
from api.models.voting import Position
from api.utils import get_items, delete_item, get_item, get_officers
from api.decorators import requires_login, requires_keys, requires_roles,\
    requires_debug

blueprint = Blueprint('users', __name__, url_prefix='/users')


@blueprint.route('/', methods=['GET'])
@requires_login
def get_all_users():
    return jsonify(get_items('users', User, request))


@blueprint.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'profile', User))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, User))


@blueprint.route('/<int:id>/', methods=['PUT'])
@requires_login
@requires_roles('webmaster')
@requires_keys('title', 'year', 'major', 'fb_id', 'bio', 'classes', 'name')
def edit_profile(id):
    json = request.get_json(force=True)
    errors = []
    user = User.query.get(id)

    if user is None:
        errors.append('Does not exist.')

    if not errors:
        user.classes = [Subject.query.get(i['id']) for i in json['classes']]
        user.bio = json['bio']
        user.major = json['major']
        user.officer_title = json['title']
        user.facebookid = json['fb_id']
        user.name = json['name']
        user.year = json['year']

    if 'roles' in json.keys():
        user.roles = [Role.query.filter_by(name=i).first()
                      for i in json['roles']]

    if 'positions' in json.keys():
        user.positions = [Position.query.get(i) for i in json['positions']]

    db.session.commit()
    return jsonify(success=not errors, errors=errors)


@blueprint.route('/fake_officer_profiles/', methods=['POST'])
@requires_debug
def fake_officer_profiles():
    for officer in get_officers():
        api.fake.fill_user_profile(officer)
    return jsonify(success=True, errors=[])


@blueprint.route('/login/', methods=['POST'])
@requires_keys('email', 'password')
def login():
    errors = []
    json = request.get_json(force=True)
    user = User.query.filter_by(email=json['email']).first()

    if user is None:
        errors.append('Invalid username/password combination')

    if not errors and not check_password_hash(user.password, json['password']):
        errors.append('Invalid username/password combination')

    if not errors:
        login_user(user, remember=False)

    return jsonify(success=not errors, errors=errors)


@blueprint.route('/logout/', methods=['GET'])
@requires_login
def logout():
    logout_user()
    return jsonify(success=True)


@blueprint.route('/', methods=['POST'])
@requires_keys('email', 'password', 'confirm', 'challenge', 'response')
def register():
    json = request.get_json(force=True)
    valid_officer = False
    errors = []
    user_id = None

    captcha_result = submit(json['challenge'], json['response'],
                            RECAPTCHA_PRIVATE_KEY, request.remote_addr)
    if not captcha_result.is_valid:
        errors.append('captcha: Validation failed.')

    if not errors:
        if '@calpoly.edu' not in json['email']:
            errors.append('You must register with a calpoly email.')

        if User.query.filter_by(email=json['email']).first():
            errors.append('An account already exists with this email.')

        if len(json['password']) < 6:
            errors.append('Password must be at least 6 characters long')

        if json['password'] != json['confirm']:
            errors.append('Passwords do not match')

    if not errors:
        user = User(
            email=json['email'],
            password=generate_password_hash(json['password'])
        )

        if valid_officer:
            user.roles.append(Role.query.filter_by(name='officer').first())
            user.roles.append(Role.query.filter_by(name='voter').first())
            user.roles.append(Role.query.filter_by(name='poster').first())
        else:
            user.roles.append(Role.query.filter_by(name='unverified').first())

        db.session.add(user)
        db.session.commit()
        user_id = user.id

        login_user(user)
    return jsonify(success=not errors, errors=errors, id=user_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count')
def fake():
    json = request.get_json(force=True)
    errors = []
    ids = []
    roles = [Role.query.filter_by(name=i).first() for i in json['roles']]\
        if 'roles' in json.keys() else None

    if roles and None in roles:
        errors.append('One of the specified roles does not exist.')

    if not errors:
        users = [api.fake.user(roles) for _ in xrange(json['count'])]
        ids = [i.id for i in users]

    return jsonify(success=not errors, errors=errors, ids=ids)
