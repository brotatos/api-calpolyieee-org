from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.users import Subject

blueprint = Blueprint('subjects', __name__, url_prefix='/subjects')


@blueprint.route('/', methods=['GET'])
@requires_login
def get_all():
    return jsonify(get_items('subjects', Subject, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Subject))
