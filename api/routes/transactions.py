import datetime
import api.fake
from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from api.core import db
from api.models.finance import BudgetCategory, Transaction
from api.utils import get_items, get_item, delete_item, OPS_REGEX
from api.decorators import requires_login, requires_keys, requires_permissions,\
    requires_debug

blueprint = Blueprint('transactions', __name__, url_prefix='/transactions')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_permissions('transactions', 'r')
def get_all():
    json = get_items('transactions', Transaction, request)
    fp = request.args.get('filter')

    if fp and ',' not in fp and 'date' in fp and ('<' in fp or '<=' in fp):
        exp = Transaction.date >= datetime.datetime\
            .fromtimestamp(float(OPS_REGEX.split(fp)[1]))
        json['next'] = [i.serialize for i in Transaction.query.filter(exp)
                        .order_by(Transaction.date)]
    return jsonify(json)


@blueprint.route('/<int:id>/', methods=['GET'])
@requires_login
@requires_permissions('transactions', 'r')
def get(id):
    return jsonify(get_item(id, 'transactions', Transaction))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_permissions('transactions', 'w')
def delete(id):
    return jsonify(delete_item(id, Transaction))


@blueprint.route('/', methods=['POST'])
@requires_login
@requires_permissions('transactions', 'w')
@requires_keys('budget_category', 'amount', 'name')
def new_transaction():
    errors = []
    json = request.get_json(force=True)
    category = BudgetCategory.query\
        .filter_by(name=json['budget_category']).first()
    date = datetime.datetime.fromtimestamp(int(json['date'])) if\
        'date' in json else datetime.datetime.now()
    transaction_id = None

    if category is None:
        errors.append('Budget category does not exist')

    if json['name'] == '':
        errors.append('Name cannot be blank')

    if not json['amount'] or int(json['amount']) == 0:
        errors.append('Amount must be non-zero')

    # Now we can create the transaction
    if not errors:
        transaction = Transaction(
            user_id=current_user.id,
            name=json['name'],
            budget_category=category,
            amount=float(json['amount']) / 100,
            date=date
        )
        db.session.add(transaction)
        db.session.commit()
        transaction_id = transaction.id

    return jsonify(success=not errors, errors=errors, id=transaction_id)


@blueprint.route('/fake/', methods=['POST'])
@requires_debug
@requires_keys('count', 'type')
def fake():
    json = request.get_json(force=True)
    errors = []
    ids = []
    category = BudgetCategory.query.filter_by(name=json['type']).first()

    if category is None:
        errors.append('Invalid budget category: `' + str(json['type']) + '`.')

    if not errors:
        transactions = [api.fake.transaction(category)
                        for _ in xrange(json['count'])]
        ids = [i.id for i in transactions]

    return jsonify(success=not errors, errors=errors, ids=ids)


@blueprint.route('/init/', methods=['POST'])
@requires_debug
def init():
    misc = BudgetCategory.query.filter_by(name='Miscellaneous').first()
    sales = BudgetCategory.query.filter_by(name='Part Sales Actual').first()
    purchases = BudgetCategory.query.filter_by(name='Direct Purchases').first()

    for _ in xrange(10):
        api.fake.transaction(misc)
        api.fake.transaction(sales)
        api.fake.transaction(purchases)

    return jsonify(success=True, errors=[])
