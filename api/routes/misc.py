from flask import Blueprint, request, jsonify
from werkzeug import check_password_hash
from api.core import db, DEBUG
from api.decorators import requires_login, requires_keys
from api.models.core import Config
from api.utils import get_categories

blueprint = Blueprint('misc', __name__, url_prefix='')


@blueprint.route('/part_categories/', methods=['GET'])
def get_part_categories():
    return jsonify(success=True, part_categories=get_categories())


@blueprint.route('/debug/', methods=['GET'])
@requires_login
def get_debug_status():
    return jsonify(success=True, debug=DEBUG)


@blueprint.route('/lounge_status/', methods=['POST'])
@requires_keys('password')
def lounge_status():
    config = Config.query.first()
    json = request.get_json(force=True)

    if check_password_hash(config.lounge_password, json['password']):
        config.lounge_open = not config.lounge_open
        db.session.commit()
        return jsonify(success=True)
    else:
        return jsonify(success=False, errors=['Invalid password.'])
