from flask import Blueprint, request, jsonify
from api.utils import delete_item, get_items
from api.decorators import requires_login, requires_roles
from api.models.parts import PartProperty

blueprint = Blueprint('part_properties', __name__,
                      url_prefix='/part_properties')


@blueprint.route('/', methods=['GET'])
@requires_login
@requires_roles('webmaster')
def get_all():
    return jsonify(get_items('part_properties', PartProperty, request))


@blueprint.route('/<int:id>/', methods=['DELETE'])
@requires_login
@requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, PartProperty))
