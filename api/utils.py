import re
import jinja2
import scrubber
import datetime
import operator
from core import db
from sqlalchemy import desc, distinct
from sqlalchemy.exc import InvalidRequestError, IntegrityError
from models.parts import Part, Kit, IC, Unit, PartProperty
from models.users import TimeSlot, User

TUTORING_START = 9
TUTORING_END = 17
DAY_END = 23


def unix_time(dt=datetime.datetime.now()):
    """
    Convert a datetime object to unix epoch
    If no argument is specified gives the current epoch time
    """
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()


def is_number(s):
    """
    Checks if a string is representing a number.
    e.g. '5' '123' "43"
    """
    try:
        float(s)
        return True
    except ValueError:
        return False


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_categories():
    return [i[0] for i in db.session.query(distinct(Part.category)).all()]


def check_addtl_info(addtlInfo):
    """
    Checks part property information regarding a specific part.
    """
    success = True

    for pair in addtlInfo:
        if 'name' not in pair.keys():
            success = False

        if 'value' in pair.keys() and (pair['value'] is None or
                                       not is_number(pair['value'])):
            success = False
        for key in pair.keys():
            if key != 'name' and key != 'value':
                success = False

    return success


def get_or_create(json, model, **kwargs):
    instance = db.session.query(model).filter_by(**kwargs).first()

    if instance:
        return instance
    else:
        instance = model(json)
        db.session.add(instance)
        db.session.commit()
        return instance


def get_part_by_id(partid):
    """
    Returns the part with that specific partid.
    """
    if partid > Kit.KIT_START_ID:
        return Kit.query.get(partid - Kit.KIT_START_ID)
    elif partid < IC.IC_START_ID:
        return Part.query.get(partid)
    else:
        return IC.query.get(partid - IC.IC_START_ID)


def to_symbol(unit):
    return {
        'Ohm': '&Omega;',
        'Farad': 'F',
        'Watt': 'W',
        'Hertz': 'Hz',
        'Volt': 'V',
        'Henry': 'H',
        }.get(unit, unit)


def engineering_notation(pair):
    """
    Takes a value (such as 100 000) and a name (such as Ohm) and returns it
    using engineering notation: 100 kOhm.
    """
    # Positive magnitude prefixes.
    posMags = ['', 'k', 'M', 'G', 'T', 'P']
    # Negative magnitude prefixes.
    # Unicode symbol is mu or micro.
    negMags = ['', 'm', u'\u03BC', 'n', 'p', 'f', 'a']
    mag = 0
    name = pair['name']
    value = pair['value'] if 'value' in pair.keys() else ''

    if value and value > 999:
        while (value > 1000):
            value /= 1000
            mag += 1

        unit = posMags[mag] + to_symbol(name)
    elif value and value < 1:
        while (value < 1):
            value *= 1000
            mag += 1

        unit = negMags[mag] + to_symbol(name)
    else:
        unit = to_symbol(name)

    return str(value) + unit


# rules about Name-Value pairs: 'name' is always required, 'value' is optional
def build_part_properties(part, addtlInfo, primary=False):
    """
    Builds the needed units and part properties from addtlInfo JSON that will
    be automatically attached to the added part.
    """
    for pair in addtlInfo:
        value = pair['value'] if 'value' in pair.keys() else None
        unit = get_or_create(pair['name'], Unit, name=pair['name'])
        part_property = PartProperty(value=value,
                                     unit_id=unit.id,
                                     part_id=part.id,
                                     primary=primary)
        db.session.add(part_property)
    db.session.commit()


TAG_REGEX = re.compile(r'<[^>]+>')
OPS_REGEX = re.compile('<=|<|>=|>|==|!=')


def sanitize(text):
    """
    Remove any harmful html by sanitizing it.
    """
    return jinja2.Markup(scrubber.Scrubber().scrub(text))


def _remove_html_tags(html):
    """
    Performs a regex substitution to remove html tags from the html param.
    e.g. '<p> foobar </p>' becomes 'foobar'
    """
    return TAG_REGEX.sub('', html)


def has_content(post_content):
    """
    Removes html tags in `post_content` and strips all whitespace from
    the post.
    """
    return bool(_remove_html_tags(post_content).replace(' ', ''))


def get_items(container, model, request):
    ops = {
        '<': operator.lt,
        '<=': operator.le,
        '>': operator.gt,
        '>=': operator.ge,
        '==': operator.eq,
        '!=': operator.ne
    }

    sort = request.args.get('sort')
    limit = request.args.get('limit')
    filters = request.args.get('filter').split(',') if\
        request.args.get('filter') else []
    desc_order = False

    response = {'errors': []}
    order_param = None
    limit_param = None
    filters_param = []

    if sort and sort[0] == '-':
        desc_order = True
        sort = sort[1:]

    if sort and sort not in model.sortable():
        response['errors'].append('Invalid sort parameter.')
    elif sort:
        order_param = desc(getattr(model, sort)) if desc_order\
            else getattr(model, sort)

    if limit and not is_int(limit):
        response['errors'].append('Invalid limit parameter.')
    elif limit:
        limit_param = int(limit)

    for s in filters:
        try:
            opr = ops[OPS_REGEX.search(s).group(0)]
        except AttributeError:
            response['errors'].append('Invalid filter parameter.')
            break

        toks = OPS_REGEX.split(s)
        if len(toks) != 2 or toks[0] not in model.sortable():
            response['errors'].append('Invalid filter parameter.')
            break

        attr = getattr(model, toks[0])
        if toks[0] == 'date' and not is_number(toks[1]):
            response['errors'].append('Invalid filter parameter.')
            break
        elif toks[0] == 'date':
            value = datetime.datetime.fromtimestamp(float(toks[1]))
        else:
            value = float(toks[1]) if is_number(toks[1]) else toks[1]

        filters_param.append(opr(attr, value))

    if not response['errors']:
        try:
            response[container] = [i.serialize for i in
                                   db.session.query(model)
                                   .filter(*filters_param)
                                   .order_by(order_param).limit(limit_param)]
        except InvalidRequestError:
            response['errors'].append('Invalid filter parameter.')

    response['success'] = not response['errors']
    return response


def get_item(item_id, container, model, user=None):
    response = {'errors': []}
    item = db.session.query(model).get(item_id)

    if not item:
        response['errors'].append('Does not exist.')
    elif user and\
            item.user_id != user.id and\
            'webmaster' not in user.get_roles():
        response['errors'].append('Forbidden.')
    else:
        response[container] = item.serialize
    response['success'] = not response['errors']
    return response


def delete_item(item_id, model, user=None):
    response = {'success': True, 'errors': []}
    item = db.session.query(model).get(item_id)

    if not item:
        response['success'] = False
        response['errors'].append('Does not exist.')
    elif user and\
            item.user_id != user.id and\
            'webmaster' not in user.get_roles():
        response['success'] = False
        response['errors'].append('Forbidden.')
    else:
        try:
            db.session.delete(item)
            db.session.commit()
        except IntegrityError:
            response['success'] = False
            response['errors'].append('Foreign key dependency.')
    return response


def get_time_slots():
    schedule = {}

    for i in range(TUTORING_START, TUTORING_END):
        schedule[i] = {}
        for slot in TimeSlot.query.filter_by(hour=i):
            schedule[i][slot.day_of_week] = slot.serialize
    return schedule


def get_officers():
    return [user for user in User.query.all() if 'officer' in
            [role.serialize for role in user.roles]]
