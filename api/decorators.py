from flask import request, jsonify, abort
from functools import wraps
from flask.ext.login import current_user
from api.models.finance import FinanceSettings
from api.core import DEBUG


def requires_login(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        if not current_user.is_authenticated():
            return jsonify(success=False,
                           errors=['Not authorized: login required'])
        return func(*args, **kwargs)
    return wrapped


def requires_debug(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        if not DEBUG:
            return abort(404)
        return func(*args, **kwargs)
    return wrapped


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if not DEBUG and (not current_user.is_authenticated() or
                              not any([True for role in current_user.roles if
                                       role.name in roles])):
                return jsonify(success=False,
                               errors=['Not authorized: requires role ' +
                                       roles[0]])
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def requires_keys(*keys):
    def wrapper(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            errors = []
            json = request.get_json(force=True)
            key_set = set(keys)

            if not key_set.issubset(set(json.keys())):
                missing = key_set.difference(set(json.keys()))
                for expected in missing:
                    errors.append('`' + expected + '` must be specified')
                return jsonify(success=False, errors=errors)
            else:
                return func(*args, **kwargs)
        return wrapped
    return wrapper


def requires_permissions(module, permissions='w'):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            pair = None

            for perm in FinanceSettings.query.filter_by(user=current_user)\
                                             .first().permissions.split(' '):
                if module == perm.split(':')[0]:
                    pair = perm.split(':')
                    break

            return f(*args, **kwargs) if (pair and permissions in pair[1]) or\
                DEBUG else jsonify(success=False, errors=['Not authorized.'])
        return decorated_function
    return decorator
