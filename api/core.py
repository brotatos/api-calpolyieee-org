from os import environ
from ast import literal_eval
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.migrate import Migrate
from flask.ext.login import LoginManager
from flask.ext.cors import CORS

DEBUG = bool(environ.get('DEBUG', False))
TESTING = bool(environ.get('TESTING', False))
SQLALCHEMY_DATABASE_URI = environ['DATABASE_URL']
SECRET_KEY = environ['SECRET_KEY']
CORS_ALLOW_HEADERS = 'Content-Type'
CORS_RESOURCES = literal_eval(environ['CORS_RESOURCES'])
CORS_SUPPORTS_CREDENTIALS = True
RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = environ['RECAPTCHA_PUBLIC_KEY']
RECAPTCHA_PRIVATE_KEY = environ['RECAPTCHA_PRIVATE_KEY']

db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()
cors = CORS()
