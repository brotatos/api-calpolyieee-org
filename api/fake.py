import datetime
from faker import Faker
from werkzeug import generate_password_hash
from random import choice, randint
from core import db
from utils import build_part_properties, get_categories, get_officers,\
    get_or_create
from models.users import User, Role, Subject
from models.posts import Tag, Post
from models.projects import Project
from models.voting import Position, Answer
from models.finance import Transaction, ReceiptItem, Receipt, BudgetCategory,\
    CashboxTotal, Reimbursement
from models.parts import IC, Part, Manufacturer, ChipType, Kit, KitItem

fake = Faker()

# These functions take in and return SQLAlchemy objects. Which means
# that error checking and getting the objects from the DB must
# be done somewhere else. Doing this makes error checking a lot easier.
# Each of these functions creates one fake object, which it returns.


# TODO: Magic numbers here, fix later
def _location(is_kit):
    loc = choice(['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'B1', 'B2', 'O']) + '-'
    top = 8 if loc[0] == 'A' else 4

    if is_kit:
        return 'O-Kit Location' + str(randint(0, 999))

    if loc[0] == 'A' or loc[0] == 'B':
        loc += str(randint(1, top)) + '-' + str(randint(1, top)) + '-'

    loc += ' '.join(fake.words(nb=3))

    return loc


def _subject():
    subj = choice(Subject.query.all())
    return subj.category + subj.class_number


def _major():
    return choice(['Computer Engineer', 'Electrical Engineer',
                   'Math Major', 'English Major'])


def _gradelevel():
    return choice(['Freshman', 'Sophomore', 'Junior', 'Senior', 'Graduate'])


def user(roles=None):
    user = User(
        email=fake.email().split('@')[0] + '@calpoly.edu',
        password=generate_password_hash('ieeeee')
    )

    user.roles = roles if roles else\
        [Role.query.filter_by(name='unverified').first()]

    db.session.add(user)
    db.session.commit()

    return user


def fill_user_profile(user):
    user.bio = fake.paragraph(nb_sentences=5)
    user.major = _major()
    user.officer_title = choice(Position.query.all()).name
    user.facebookid = user.email.split('@')[0]
    user.name = fake.name()
    user.year = _gradelevel()
    db.session.commit()


def _static_part_json(type):
    return {
        'RESISTOR': {
            'category': 'RESISTOR',
            'stock': randint(0, 9),
            'price': 10,
            'main_info': {
                'value': randint(0, 999) * pow(10, randint(0, 4)),
                'name': 'Ohm'
            },
            'addtl_info': [
                {
                    'value': '0.25' if choice([True, False]) else '0.5',
                    'name': 'Watt'
                }
            ]
        },
        'CAPACITOR': {
            'category': 'CAPACITOR',
            'stock': randint(0, 9),
            'price': 50,
            'main_info': {
                'value': randint(0, 999) * pow(10, randint(0, 7) - 10),
                'name': 'Farad'
            },
            'addtl_info': [
                {
                    'name': choice(['Electrolytic', 'Ceramic', 'Aluminum'])
                }
            ]
        },
        'DIODE': {
            'category': 'DIODE',
            'price': 100,
            'main_info': {
                'name': '1N3600' if choice([True, False]) else '3mm Red'
            }
        },
        'POT': {
            'category': 'POT',
            'price': 100,
            'main_info': {
                'value': choice([100, 500, 1000, 25000, 100000, 250000]),
                'name': 'Ohm'
            },
            'addtl_info': [
                {
                    'value': 25,
                    'name': 'turn'
                }
            ]
        }
    }[type]


def static_part(type):
    json = _static_part_json(type)
    part = Part(json['category'], json['price'])

    db.session.add(part)
    db.session.commit()

    build_part_properties(part, [json['main_info']], True)
    if 'addtl_info' in json.keys():
        build_part_properties(part, json['addtl_info'])

    return part


def ic():
    ic = IC(
        name=' '.join(fake.words(nb=3)),
        stock=randint(0, 9),
        price=randint(0, 999),
        location=_location(False),
        mftr_id=choice(Manufacturer.query.all()).id,
        mftr_part_no=fake.state_abbr()+fake.postcode(),
        chip_type_id=choice(ChipType.query.all()).id,
        addtlInfo=''
    )
    db.session.add(ic)
    db.session.commit()
    part_id = ic.id + IC.IC_START_ID
    part = Part(category='IC', price=ic.price, id=part_id)
    ic.part_id = part_id
    db.session.add(part)
    db.session.commit()

    return ic


def kit(title=None):
    part_categories = get_categories()
    kit_items = []
    kit = Kit(
        name=title if title else _subject(),
        stock=randint(0, 19),
        price=randint(2000, 3000),
        location=_location(True),
        kit_items=[]
    )
    db.session.add(kit)
    db.session.commit()

    for cat in part_categories:
        for _ in xrange(3):
            kit_items.append({
                'id': choice(Part.query.filter_by(category=cat).all()).id,
                'quantity': randint(1, 5)
            })

    kit.kit_items = [KitItem(kit_id=kit.id,
                             part_id=item['id'],
                             quantity=item['quantity'])
                     for item in kit_items]
    db.session.commit()

    return kit


def transaction(category):
    rand = randint(0, 9)

    if category.name == 'Part Sales Actual':
        name = 'Deposit'
        amount = choice([1000.00, 1250.00, 2000.00, 500.00])
    elif category.name == 'Miscellaneous' and rand < 2:
        name = 'IRA Club Funding'
        amount = 3300.00
    elif category.name == 'Miscellaneous':
        name = 'Use Tax'
        amount = float(randint(-10000, -1000)) / 100
    elif category.name == 'Direct Purchases':
        name = choice(['Mouser', 'Digikey', 'Adafruit'])
        amount = float(randint(-100000, -3000)) / 100

    transaction = Transaction(
        user_id=choice(get_officers()).id,
        name=name,
        budget_category=category,
        amount=amount,
        date=fake.date_time_between(start_date="-1y", end_date="now")
    )
    db.session.add(transaction)
    db.session.commit()

    return transaction


def receipt(user=None):
    part_sales_bc = BudgetCategory.query\
        .filter_by(name='Part Sales').first()
    user_id = user.id if user else choice(get_officers()).id
    items = []

    for _ in xrange(randint(1, 10)):
        item = None
        while item is None or item in items:
            item = choice(Part.query.all())
        items.append(item)

    transaction = Transaction(
        user_id=user_id,
        budget_category=part_sales_bc,
        amount=float(sum([i.price for i in items])) / 100,
        date=datetime.datetime.now()
    )

    db.session.add(transaction)
    db.session.commit()

    receipt = Receipt(
        seller_id=user_id,
        transaction_id=transaction.id,
        purchaser=fake.name() if choice([True, False]) else '',
        comment=fake.paragraph(nb_sentences=4) if choice([True, False]) else ''
    )

    db.session.add(receipt)
    db.session.commit()

    # TODO: Fix the quantity issue.
    for item in items:
        receipt_item = ReceiptItem(
            receipt_id=receipt.id,
            ieee_part_no=item.id,
            quantity=1
        )
        db.session.add(receipt_item)
    db.session.commit()

    return receipt


def cashbox_total():
    total = CashboxTotal(
        amount=float(randint(20000, 40000)) / 100,
        date=fake.date_time_between(start_date='-1y', end_date='now')
    )
    db.session.add(total)
    db.session.commit()

    return total


def project(user, is_completed=None):
    title = ' '.join(fake.words(nb=randint(1, 5)))
    url = title.replace(' ', '_').lower()
    project = Project(
        title=title.title(),
        completed=is_completed if is_completed else choice([True, False]),
        repo='',
        contact=user if user else choice(get_officers()),
        image='http://lorempixel.com/800/600/cats/',
        caption='',
        description=fake.sentence(),
        content='<div><p>' +
        '</p><p>'.join(fake.paragraphs(nb=randint(5, 9))) +
        '</p></div>',
        url=url,
        tag=get_or_create(url, Tag, name=url)
    )
    db.session.add(project)
    db.session.commit()

    return project


def reimbursement(user=None, type=None):
    reimbursement = Reimbursement(
        requester_id=user.id if user else choice(get_officers()).id,
        amount=float(randint(0, 99)),
        type=type if type else 'REGULAR',
        comment=fake.paragraph(nb_sentences=3),
        date_requested=datetime.datetime.now()
    )
    db.session.add(reimbursement)
    db.session.commit()

    return reimbursement


def post(user=None):
    tags = fake.words(nb=randint(1, 5))
    images = []

    for _ in xrange(randint(0, 4)):
        images.append('http://lorempixel.com/640/480/cats/')

    post = Post({
        'title': ' '.join(tags).title(),
        'content': '<div><p>' +
                   '</p><p>'.join(fake.paragraphs(nb=randint(5, 9))) +
                   '</p></div>',
        'tags': [{'name': tag} for tag in tags],
        'images': images
    }, user.id if user else choice(User.query.all()).id)

    db.session.add(post)
    db.session.commit()

    return post


def answer(question, user, position):
    answer = Answer(
        question_id=question.id,
        user_id=user.id,
        position_id=position.id,
        answer=fake.paragraph(nb_sentences=5)
    )

    db.session.add(answer)
    db.session.commit()

    return answer
