DELETE FROM console;
INSERT INTO console (filename, title, description) VALUES
('posts'      , 'Posts'          , 'Post news and events'      ),
('profile'    , 'Profile'        , 'Modify your profile'       ),
('front_page' , 'Front Page'     , 'Modify the front page'     ),
('config'     , 'Config'         , 'Configure site variables'  ),
('voting'     , 'Voting'         , 'Elect new officers'        ),
('projects'   , 'Projects'       , 'Add or edit projects'      ),
('tutoring'   , 'Tutoring'       , 'Set tutoring hours'        ),
('finance'    , 'Reimbursements' , 'Requests for Reimbursement'),
('run'        , 'Run'            , 'Run for a position'        );

DELETE FROM role;
INSERT INTO role (name) VALUES
('webmaster'),
('poster'),
('voter'),
('officer'),
('treasurer'),
('secretary'),
('publicity'),
('parts_manager'),
('unverified'),
('delete_eligible'),
('old_officer'),
('parts_seller');

DELETE FROM role_console;
INSERT INTO role_console (role_id, console_id) VALUES
((SELECT id FROM role WHERE name='webmaster'),       (SELECT id FROM console WHERE filename='config')),
((SELECT id FROM role WHERE name='poster'),          (SELECT id FROM console WHERE filename='posts')),
((SELECT id FROM role WHERE name='voter'),           (SELECT id FROM console WHERE filename='voting')),
((SELECT id FROM role WHERE name='voter'),           (SELECT id FROM console WHERE filename='run')),
((SELECT id FROM role WHERE name='officer'),         (SELECT id FROM console WHERE filename='profile')),
((SELECT id FROM role WHERE name='officer'),         (SELECT id FROM console WHERE filename='finance')),
((SELECT id FROM role WHERE name='secretary'),       (SELECT id FROM console WHERE filename='tutoring')),
((SELECT id FROM role WHERE name='publicity'),       (SELECT id FROM console WHERE filename='front_page')),
((SELECT id FROM role WHERE name='publicity'),       (SELECT id FROM console WHERE filename='projects'));

DELETE FROM manufacturer;
INSERT INTO manufacturer (name) VALUES
('MARVELL'),
('BROADCOM'),
('MAXIM'),
('RENESAS'),
('TEXAS INSTRUMENTS'),
('FAIRCHILD SEMICONDUCTOR'),
('ANALOG DEVICES'),
('SAMSUNG'),
('NATIONAL SEMICONDUCTOR'),
('LATTICE SEMICONDUCTOR'),
('MINI CIRCUITS'),
('HARRIS SEMICONDUCTOR'),
('OTHER');

DELETE FROM chip_type;
INSERT INTO chip_type (name) VALUES
('DIP'),
('TSSOP'),
('PLCC'),
('QFP'),
('Other');

DELETE FROM discount;
INSERT INTO discount (name, percent) VALUES
('IEEE Member', 20.0),
('Refund'     , 200.0);

DELETE FROM position;
INSERT INTO position (name, multiple, description) VALUES
('Branch President'       , 1, '(Must be a Current Officer) – Leads the club.  Runs weekly meetings and delegates tasks to officers. Has final word with most of, if not all decisions.  Works closely with both vice presidents to ensure everything runs smoothly.'),
('Vice President External', 1, '(Must be a Current Officer)– Handles both the inter-club and inter-college (IEEE Central Coast Section, EE Clubs) relations.  Keep checks and balances with the President and fills in for the President when necessary. Provides oversight for Corporate Relations and events.'),
('Vice President Internal', 1, '(Must be a Current Officer) – Handles the inter-officer, club-organization, and EE department relations.  Keep checks and balances with the President and fills in for the President when necessary. Provides oversight for Project Managers and events.'),
('Secretary'              , 1, 'Takes meeting minutes at every officer meeting. Also creates the bi-weekly newsletter, and puts together any other publications. Keeps the club calendar updated, both online (Google calendar) and inside the club (whiteboard).'),
('Treasurer'              , 1, 'Handles all money going through IEEE. Works closely with Lani to keep track of the account balance. Approves all purchases, and assists students with reimbursements.'),
('Webmaster'              , 1, 'Maintains and updates the website when necessary. Keeps the projects section, wiki, and parts database up to date.'),
('Workshop Coordinator'   , 2, 'Develops small projects to make available for students. Continues stock materials and help with previously developed small projects. Hosts open lab hours for students to work on small projects.'),
('Publicity Manager'      , 1, 'Creates, prints, and posts flyers to advertise for every event. Creates Facebook events, and finds other creative solutions to advertise for IEEE activities including general meetings, projects, info sessions, competitions, etc.'),
('Corporate Relations'    , 1, 'Writes and sends the IEEE proposal to industry. Coordinates guest speakers from industry to come to Cal Poly for tech-talks or info-sessions. Plans and leads Winter Break Industry Tour trip.'),
('Parts Manager'          , 1, 'Orders parts quarterly for lab kits and to keep the parts cabinet stocked. Puts together lab kits every quarter for available courses. Tracks sales and works closely with Lani to place necessary orders. Keeps track of the parts database.'),
('Events Coordinator'     , 2, 'Sets up the social activities for the student body such as movie nights, hikes, and game board nights. Plans and organizes large events such as WOW Week, LAN Party, Bowling Night, and works with Outreach Coordinator for Open House. Works with companies to host tech talks and info sessions for the students. Supports networking with industry.');

DELETE FROM subjects;
INSERT INTO subjects (category, class_number) VALUES
 ('CHEM', '124'),
 ('CPE' , '101'),
 ('CPE' , '102'),
 ('CPE' , '103'),
 ('CPE' , '133'),
 ('CPE' , '233'),
 ('CPE' , '315'),
 ('CPE' , '329'),
 ('CPE' , '464'),
 ('CSC' , '141'),
 ('EE'  , '112'),
 ('EE'  , '211'),
 ('EE'  , '212'),
 ('EE'  , '228'),
 ('EE'  , '255'),
 ('EE'  , '328'),
 ('EE'  , '306'),
 ('EE'  , '307'),
 ('EE'  , '308'),
 ('MATH', '141'),
 ('MATH', '142'),
 ('MATH', '143'),
 ('MATH', '241'),
 ('MATH', '244'),
 ('PHYS', '141'),
 ('PHYS', '133'),
 ('PHYS', '132'),
 ('PHYS', '211');

DELETE FROM question;
INSERT INTO question (prompt) VALUES
('Why are you the right candidate for this position?'),
('What are your goals for this position next year?'),
('Where do you want to see IEEE - SB in the future?');

DELETE FROM budget_category;
INSERT INTO budget_category (name, type) VALUES
('Reimbursements'   , 'TRACKING'),
('Part Sales'       , 'NON_TRACKING'),
('Part Sales Actual', 'TRACKING'),
('Miscellaneous'    , 'TRACKING'),
('Direct Purchases' , 'TRACKING');

INSERT INTO imagelink (link)
SELECT 'http://static.calpolyieee.org/img/resistors_small.jpg'
WHERE NOT EXISTS (
   SELECT link from imagelink WHERE link = 'http://static.calpolyieee.org/img/resistors_small.jpg'
);

INSERT INTO imagelink (link)
SELECT 'http://static.calpolyieee.org/img/group_photo.jpg'
WHERE NOT EXISTS (
   SELECT link from imagelink WHERE link = 'http://static.calpolyieee.org/img/group_photo.jpg'
);

INSERT INTO users_user (name, email)
SELECT 'WRITE_IN', 'WRITE_IN'
WHERE NOT EXISTS (
   SELECT name from users_user WHERE name = 'WRITE_IN'
);

DELETE FROM config;
INSERT INTO config
(voting_started,
lounge_open,
run_console_visible,
voting_end_date,
landing_image_id,
officers_image_id)
VALUES
(false,
false,
true,
NOW(),
(SELECT id FROM imagelink WHERE link='http://static.calpolyieee.org/img/resistors_small.jpg'),
(SELECT id FROM imagelink WHERE link='http://static.calpolyieee.org/img/group_photo.jpg'));

DROP FUNCTION IF EXISTS webmaster(character varying(50));
CREATE FUNCTION webmaster(character varying(50)) RETURNS void AS $$
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='webmaster')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='poster')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='voter')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='officer')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='treasurer')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='secretary')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='publicity')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='parts_manager')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='delete_eligible')
   );
   INSERT INTO user_role VALUES (
      (SELECT id FROM users_user WHERE email=$1), (SELECT id FROM role WHERE name='parts_seller')
   );
   DELETE FROM user_role WHERE user_id =
      (SELECT id FROM users_user WHERE email=$1)
    AND role_id = (SELECT id FROM role WHERE name='unverified');
$$ LANGUAGE SQL;
